Hello
-----

There are two main ways to use neo-python: ``np-prompt`` and running just the node with custom
code.

np-prompt
"""""""""

Start np-prompt on TestNet:

::

    $ np-prompt

Show help with all available arguments:

::

    $ np-prompt -h
    usage: np-prompt [-h] [-m | -p [host] | --coznet | -c CONFIG]
                     [-t {dark,light}] [-v] [--datadir DATADIR] [--version]

    optional arguments:
      -h, --help            show this help message and exit
      -m, --mainnet         Use MainNet instead of the default TestNet
      -p [host], --privnet [host]
                            Use a private net instead of the default TestNet,
                            optionally using a custom host (default: 127.0.0.1)
      --coznet              Use the CoZ network instead of the default TestNet
      -c CONFIG, --config CONFIG
                            Use a specific config file
      -t {dark,light}, --set-default-theme {dark,light}
                            Set the default theme to be loaded from the config
                            file. Default: 'dark'
      -v, --verbose         Show smart-contract events by default
      --datadir DATADIR     Absolute path to use for database directories
      --version             show program's version number and exit


Node with custom code
"""""""""""""""""""""

Take a look at the examples in the ``/examples`` directory: https://github.com/CityOfZion/neo-python/tree/development/examples

See also the sections about "Settings and Logging" and "Interacting with Smart Contracts".


API server (JSON and/or REST)
""""""""""""""""""""""""""""""

Start JSON and REST API Server on Mainnet:

::

    $ np-api-server --mainnet --port-rpc 10332 --port-rest 80

Example notifications plus help with all available arguments:

::

  $ np-api-server --testnet --port-rpc 8080 --port-rest 8088


Port Description
""""""""""""""""""""""""""""""

If you want an external program to access your API server, an open firewall port is required. The following is a port description that can be set to fully open or open-on-demand.

.. list-table:: Port Description
   :widths: 20 10 10
   :header-rows: 1

   * -
     - Main Net
     - Test Net
   * - JSON-RPC via HTTPS
     - 10331
     - 20331
   * - JSON-RPC via HTTP
     - 10332
     - 20332


Running an API Server using Windows WSL (Ubuntu)
""""""""""""""""""""""""""""""""""""""""""""""""

If you are running neo-python on Windows WSL (Ubuntu), opening the respective ports on your router is not enough.
You will need to add a new inbound rule to your Windows Firewall as described `here <https://www.nextofwindows.com/allow-server-running-inside-wsl-to-be-accessible-outside-windows-10-host>`_.

from nash.graphql_schema import graphql_schema


class Query(graphql_schema.RootQueryType):
    """ This is needed because sgqlc requires the mutation class be called exactly
    Query, and our API calls it RootQueryType, therefore the schema generator
    creates it that way too."""
    pass


class Mutation(graphql_schema.RootMutationType):
    """ This is needed because sgqlc requires the mutation class be called exactly
    Mutation, and our API calls it RootMutationType, therefore the schema generator
    creates it that way too.

    If the sgqlc operation type (class name) is not exactly 'Mutation' it will
    result in a query. """
    pass


def add_market_fields(gql_market_req, with_assets=False):
    """ Add all fields for a market object """
    gql_market_req.__fields__(
        id=True,
        a_unit=True,
        a_unit_precision=True,
        b_unit=True,
        b_unit_precision=True,
        min_tick_size=True,
        min_trade_size=True,
        min_trade_size_b=True,
        min_trade_increment=True,
        min_trade_increment_b=True,
        name=True,
        status=True
    )

    if with_assets:
        gql_market_req.__fields__(
            a_asset=True,
            b_asset=True
        )
        add_asset_fields(gql_market_req.a_asset)
        add_asset_fields(gql_market_req.b_asset)


def add_asset_fields(gql_asset_req):
    """ Add all fields for a asset object """
    gql_asset_req.__fields__(
        blockchain=True,  # String
        blockchain_precision=True,  # Int
        deposit_precision=True,  # Int
        hash=True,  # String
        name=True,  # String
        symbol=True,  # CurrencySymbol
        withdrawal_precision=True,  # Int
    )


def add_order_fields(gql_order_req):
    """ Add all fields for an order object """
    gql_order_req.__fields__(
        amount=True, buy_or_sell=True, cancel_at=True, cancellation_policy=True,
        id=True, limit_price=True, market=True,
        placed_at=True, status=True, stop_price=True, trades=True, type=True
    )
    gql_order_req.amount.__fields__(
        amount=True,
        currency=True
    )
    gql_order_req.amount_remaining.__fields__(
        amount=True,
        currency=True
    )
    gql_order_req.limit_price.__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_order_req.market.__fields__(
        a_unit=True,
        a_unit_precision=True,
        b_unit=True,
        b_unit_precision=True,
        min_tick_size=True,
        min_trade_size=True,
        min_trade_size_b=True,
        name=True,
        status=True
    )
    gql_order_req.stop_price.__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_order_req.trades.__fields__(
        amount=True,
        executed_at=True,
        id=True,
        limit_price=True,
        market=True
    )

    gql_order_req.trades.amount.__fields__(
        amount=True,
        currency=True
    )
    gql_order_req.trades.limit_price.__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_order_req.trades.market.__fields__(
        a_unit=True,
        a_unit_precision=True,
        b_unit=True,
        b_unit_precision=True,
        min_tick_size=True,
        min_trade_size=True,
        min_trade_size_b=True,
        name=True,
        status=True
    )


def add_ticker_fields(gql_ticker_req):
    """ Add all fields for a market object """
    gql_ticker_req.id()
    gql_ticker_req.market_name()

    gql_ticker_req.best_ask_price().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.best_ask_size().__fields__(
        amount=True,
        currency=True
    )
    gql_ticker_req.best_bid_price().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.best_bid_size().__fields__(
        amount=True,
        currency=True
    )
    gql_ticker_req.high_price24h().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.last_price().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.low_price24h().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.market().__fields__(
        a_unit=True,
        a_unit_precision=True,
        b_unit=True,
        b_unit_precision=True,
        min_tick_size=True,
        min_trade_size=True,
        min_trade_size_b=True,
        name=True,
        status=True
    )
    gql_ticker_req.price_change24h().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.price_change24h_pct()
    gql_ticker_req.usd_last_price().__fields__(
        amount=True,
        currency_a=True,
        currency_b=True
    )
    gql_ticker_req.volume24h().__fields__(
        amount=True,
        currency=True
    )

from nash import graphql_helper
from nash.graphql_schema import DateTime, CandleInterval, OrderHistory, ExchangeStatus, Market, OrderBook, CandleRange, Ticker, OrderStatus, OrderBuyOrSell, OrderType, TradeHistory, Asset
from nash.graphql_helper import Query, add_ticker_fields

from sgqlc.operation import Operation
from sgqlc.types import list_of


class GraphQlUnsignedRequestsMixin:
    """ Used as Mixin for NashApi """

    def get_exchange_public_key(self) -> str:
        """
        Get the public key of the exchange

        Examples:

            >>> api.get_exchange_public_key()
            '02e8973fb4ef7e09ed138c9d8f9c1ca2733ba9a2a3ef2af684cb09a53eaed4ba26'

        Returns:
            str: The public key of the exchange
        """
        op = Operation(Query)
        pubkey = op.get_exchange_public_key()
        pubkey.public_key()
        res = self._exec_gql_query(op)
        return res.get_exchange_public_key.public_key

    def get_exchange_status(self) -> ExchangeStatus:
        """
        Get current exchange status

        Examples:

            >>> api.get_exchange_status()
            ExchangeStatus(status='running', server_timestamp=1551789721600, api_limits=[ApiLimit(request_name='candles', time_period='minutely', max_calls_per_time_period=20)])

        Returns:
            ExchangeStatus: The current status of the exchange
        """
        op = Operation(Query)
        req = op.get_exchange_status()
        req.status()
        req.server_timestamp()
        req.api_limits.__fields__(
            request_name=True,
            time_period=True,
            max_calls_per_time_period=True,
        )

        # print(op)  # prints the gql query
        res = self._exec_gql_query(op)
        # print(data)
        return res.get_exchange_status

    def list_markets(self, with_assets=False) -> list_of(Market):
        """ Gets a list of all markets.

        Args:
            with_assets (bool): Whether to include the full objects for a_asset and b_asset (default=False)

        Examples:
            >>> api.list_markets()
            [Market(id='market:eth_neo', a_unit='eth', a_unit_precision=8, b_unit='neo', b_unit_precision=8, min_tick_size='0.001', min_trade_size='0.02000000', min_trade_size_b='0.50000000', min_trade_increment='0.00001', min_trade_increment_b='0.001', name='eth_neo', status='RUNNING'),
            Market(id='market:eth_bat', a_unit='eth', a_unit_precision=7, b_unit='bat', b_unit_precision=7, min_tick_size='0.01', min_trade_size='0.0200000', min_trade_size_b='10.0000000', min_trade_increment='0.00001', min_trade_increment_b='0.01', name='eth_bat', status='RUNNING'),
            ...]

        Returns:
            [Market]: a list of markets
        """
        op = Operation(Query)
        req = op.list_markets()
        graphql_helper.add_market_fields(req, with_assets=with_assets)

        res = self._exec_gql_query(op)
        return res.list_markets

    def get_market(self, market_name: str, with_assets=False) -> Market:
        """ Gets a market by name.

        Args:
            market_name: name of market to get details for
            with_assets (bool): Whether to include the full objects for a_asset and b_asset (default=False)

        Examples:
            >>> api.get_market('gas_neo')
            Market(a_unit='gas', a_unit_precision=8, b_unit='neo', b_unit_precision=0, min_tick_size='1.0e-6', min_trade_size='1.0e-6', name='gas_neo', status='RUNNING')

        Returns:
            Market: a specific market
        """
        op = Operation(Query)
        req = op.get_market(market_name=market_name)
        graphql_helper.add_market_fields(req, with_assets=with_assets)

        res = self._exec_gql_query(op)
        return res.get_market

    def get_order_book(self, market_name: str) -> OrderBook:
        """
        Gets an order book by market name.

        Examples:
            >>> api.get_order_book('eth_neo')

        Args:
            market_name: name of market to get orders for

        Returns:
            OrderBook: the order book
        """

        op = Operation(Query)
        req = op.get_order_book(market_name=market_name)
        req.__fields__(
            asks=True,
            bids=True,
            market=True,
            update_id=True
        )

        req.asks.__fields__(
            amount=True,
            price=True
        )

        req.asks.amount.__fields__(
            amount=True,
            currency=True
        )
        req.asks.price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )

        req.bids.__fields__(
            amount=True,
            price=True
        )
        req.bids.amount.__fields__(
            amount=True,
            currency=True
        )
        req.bids.price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )
        graphql_helper.add_market_fields(req.market)

        res = self._exec_gql_query(op)
        return res.get_order_book

    def list_tickers(self) -> list_of(Ticker):
        """
        Gets current tickers

        Returns:
            [Ticker]:
        """

        op = Operation(Query)
        req = op.list_tickers()
        add_ticker_fields(req)

        res = self._exec_gql_query(op)
        return res.list_tickers

    def list_candles(self,
                     market_name: str,
                     before: DateTime = None,
                     interval: CandleInterval = CandleInterval('ONE_MINUTE'),
                     limit: int = 25) -> CandleRange:
        """
        Gets candles for a market

        Args:
            market_name (str): Market to list candles for (e.g. 'eth_neo')
            before (iso8601 date, optional): DateTime before which to list candles
            interval (str/CandleInterval, optional): CandleInterval
            limit (int, optional): max number of candles to retrieve

        Returns:
            CandleRange:

        Examples:

            >>> api.list_candles('gas_neo')
            CandleRange(candles=[Candle(a_volume=CurrencyAmount(amount='210.00000000', currency='gas'), b_volume=CurrencyAmount(amount='10', currency='neo'), close_price=CurrencyPrice(amount='0.047619', currency_a='neo', currency_b='gas'), high_price=CurrencyPrice(amount='0.047619', currency_a='neo', currency_b='gas'), interval='ONE_MINUTE', interval_starting_at=datetime.datetime(2019, 2, 28, 17, 49, tzinfo=datetime.timezone.utc), low_price=CurrencyPrice(amount='0.047619', currency_a='neo', currency_b='gas'), open_price=CurrencyPrice(amount='0.047619', currency_a='neo', currency_b='gas')), Candle(a_volume=CurrencyAmount(amount='22797.00000000', currency='gas'), b_volume=CurrencyAmount(amount='1714', currency='neo'), close_price=CurrencyPrice(amount='0.071428', currency_a='neo', currency_b='gas'), high_price=CurrencyPrice(amount='0.071428', currency_a='neo', currency_b='gas'), interval='ONE_MINUTE', interval_starting_at=datetime.datetime(2019, 2, 28, 17, 48, tzinfo=datetime.timezone.utc), low_price=CurrencyPrice(amount='0.083333', currency_a='neo', currency_b='gas'), open_price=CurrencyPrice(amount='0.071428', currency_a='neo', currency_b='gas'))])
        """

        op = Operation(Query)
        req = op.list_candles(market_name=market_name, before=before, interval=interval, limit=limit)
        req.candles().__fields__(
            a_volume=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='aVolume')
            b_volume=True,  # = sgqlc.types.Field('CurrencyAmount', graphql_name='bVolume')
            close_price=True,  # = sgqlc.types.Field('CurrencyPrice', graphql_name='closePrice')
            high_price=True,  # = sgqlc.types.Field('CurrencyPrice', graphql_name='highPrice')
            interval=True,  # = sgqlc.types.Field(CandleInterval, graphql_name='interval')
            interval_starting_at=True,  # = sgqlc.types.Field(DateTime, graphql_name='intervalStartingAt')
            low_price=True,  # = sgqlc.types.Field('CurrencyPrice', graphql_name='lowPrice')
            open_price=True,  # = sgqlc.types.Field('CurrencyPrice', graphql_name='openPrice')

        )
        req.candles.a_volume.__fields__(
            amount=True,
            currency=True
        )
        req.candles.b_volume.__fields__(
            amount=True,
            currency=True
        )
        req.candles.close_price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )
        req.candles.high_price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )
        req.candles.low_price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )
        req.candles.open_price.__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )

        res = self._exec_gql_query(op)
        return res.list_candles

    def list_orders(self,
                    market_name: str,
                    before: DateTime = None,
                    limit: int = 50,
                    status: list_of(OrderStatus) = None,
                    buy_or_sell: OrderBuyOrSell = None,
                    range_start: DateTime = None,
                    range_stop: DateTime = None,
                    order_types: list_of(OrderType) = None) -> OrderHistory:
        """
        Gets orders for a market

        Args:
            market_name (str): Market to list orders for (e.g. 'eth_neo')
            before (iso8601 date, optional): DateTime before which to list candles
            limit (int, optional): max number of candles to retrieve
            status ([OrderStatus], optional): status of orders to query
            buy_or_sell (OrderBuyOrSell, optional): 'BUY' or 'SELL'
            range_start (DateTime, optional): start of orders to retrieve
            range_stop (DateTime, optional): end of orders to retrieve
            order_types ([OrderType], optional): 'LIMIT', 'MARKET', 'STOP_LIMIT', 'STOP_MARKET'

        Returns:
            OrderHistory:
        """
        # Currenty does not work
        # Gets an error simalar to  "Opaque pagination cursor. Value is returned as `next` value in previous response."
        op = Operation(Query)
        args = {"market_name": market_name}
        if before is not None:
            args["before"] = before
        if limit is not None:
            args["limit"] = limit
        if status is not None:
            args["status"] = status
        if buy_or_sell is not None:
            args["buy_or_sell"] = buy_or_sell
        if range_start is not None:
            args["range_start"] = range_start
        if range_stop is not None:
            args["range_stop"] = range_stop
        if order_types is not None:
            args["type"] = order_types
        req = op.list_orders(**args)

        req.next()
        graphql_helper.add_order_fields(req.orders)

        res = self._exec_gql_query(op)
        return res.list_orders

    def list_trades(self,
                    market_name: str,
                    before: DateTime = None,
                    limit: int = 50) -> TradeHistory:
        """
        Lists all trades for a market

        Args:
            market_name: Market to list trades for (e.g. 'eth_neo')
            before: DateTime before which to list candles
            limit: max number of trades to retrieve

        Returns:
            TradeHistory:
        """
        args = {"market_name": market_name}
        if before is not None:
            args["before"] = before
        if limit is not None:
            args["limit"] = limit

        op = Operation(Query)
        req = op.list_trades(**args)
        req.next()
        req.trades().__fields__(
            amount=True,  # = sgqlc.types.Field(CurrencyAmount, graphql_name='amount')
            executed_at=True,  # = sgqlc.types.Field(DateTime, graphql_name='executedAt')
            id=True,  # = sgqlc.types.Field(ID, graphql_name='id')
            limit_price=True,  # = sgqlc.types.Field(CurrencyPrice, graphql_name='limitPrice')
            market=True  # = sgqlc.types.Field(Market, graphql_name='market')
        )
        req.trades.amount.__fields__(
            amount=True,
            currency=True
        )
        req.trades().limit_price().__fields__(
            amount=True,
            currency_a=True,
            currency_b=True
        )
        graphql_helper.add_market_fields(req.trades.market)

        res = self._exec_gql_query(op)
        return res.list_trades

    def get_ticker(self, market_name: str) -> Ticker:
        """
        Get ticker for a market

        Args:
            market_name: Market to get ticker for (e.g. 'eth_neo')

        Returns:
            Ticker:
        """
        op = Operation(Query)
        req = op.get_ticker(market_name=market_name)
        add_ticker_fields(req)

        res = self._exec_gql_query(op)
        return res.get_ticker

    def list_assets(self) -> list_of(Asset):
        """
        Lists all assets

        Returns:
            [Asset]:

        Example:
            >>> api.list_assets()
            [Asset(blockchain='eth', hash='0d8775f648430679a709e98d2b0cb6250d2887ef', name='BAT', precision=18, symbol='bat', tradeable=True, withdrawal_precision=18),
            Asset(blockchain='eth', hash='0000000000000000000000000000000000000000', name='Ethereum', precision=18, symbol='eth', tradeable=True, withdrawal_precision=18),
            Asset(blockchain='neo', hash='602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7', name='Gas', precision=8, symbol='gas', tradeable=True, withdrawal_precision=8),
            Asset(blockchain='neo', hash='bb3b54ab244b3658155f2db4429fc38ac4cef625', name='Moonlight Lux', precision=8, symbol='lx', tradeable=True, withdrawal_precision=8),
            Asset(blockchain='neo', hash='1a16b760432e28aca03a76e9afe5522e9acad781', name='NEX Token', precision=8, symbol='nex', tradeable=True, withdrawal_precision=8),
            ...
        """
        op = Operation(Query)
        req = op.list_assets()
        graphql_helper.add_asset_fields(req)

        res = self._exec_gql_query(op)
        return res.list_assets

#!/usr/bin/env python3
"""
This cli utility will be installed as `nash-utils`. To use it, just run `nash-utils` in your shell.
You can use nash-utils to do several API requests. The output is in JSON format to stdout.

You can also use it with staging:

    NASH_ENV=staging nash-utils

To enable additional debug information:

    DEBUG=1 nash-utils
"""
import argparse
import json

from pprint import pprint
from nash import __version__
from nash import NashApi
import nash.nash_core as nash_core


def main():
    parser = argparse.ArgumentParser()

    # Show version
    go_core_version = nash_core.version()
    version_str = "Nash Python API Client v{version} (Go crypto core v{go_version})".format(version=__version__, go_version=go_core_version)
    parser.add_argument("--version", action="version", version=version_str)

    actions_group = parser.add_argument_group("Actions")
    group = actions_group.add_mutually_exclusive_group(required=True)
    group.add_argument("--exchange-status", action='store_true', help="Get the exchange status")
    group.add_argument("--exchange-pubkey", action='store_true', help="Get the exchange public key")
    group.add_argument("--list-assets", action='store_true', help="List all assets")
    group.add_argument("--get-asset", action='store', dest="get_asset_name", metavar="asset-name", help="Get a specific asset (e.g. neo or travala)")
    group.add_argument("--list-markets", action='store_true', help="List all markets")
    group.add_argument("--list-market-names", action='store_true', help="List all markets (name only)")
    group.add_argument("--get-market", action='store', dest="get_market_name", metavar="market-name", help="Get details of a specific markets (e.g. gas_neo)")

    # --json can be used for all actions to produce JSON output
    parser.add_argument("--json", action='store_true', help="Output in json")

    args = parser.parse_args()
    json_output = args.json
    # print(args)

    api = NashApi()

    if args.exchange_status:
        status = api.get_exchange_status()
        if json_output:
            res_obj = status.__to_json_value__()
            print(json.dumps(res_obj, sort_keys=True, indent=2))
        else:
            pprint(status)

    elif args.exchange_pubkey:
        pubkey = api.get_exchange_public_key()
        if json_output:
            res_obj = {"publicKey": pubkey}
            print(json.dumps(res_obj, sort_keys=True, indent=2))
        else:
            print(pubkey)

    elif args.list_markets:
        markets = api.list_markets()
        if json_output:
            res_obj = [market.__to_json_value__() for market in markets]
            print(json.dumps(res_obj, sort_keys=True, indent=2))
        else:
            pprint(markets)

    elif args.list_market_names:
        markets = api.list_markets()
        res_obj = [market.name for market in markets]
        print(json.dumps(res_obj, sort_keys=True, indent=2))

    elif args.get_market_name:
        market = api.get_market(args.get_market_name)
        if json_output:
            res_obj = market.__to_json_value__()
            print(json.dumps(res_obj, sort_keys=True, indent=2))
        else:
            print(market)

    elif args.get_asset_name:
        q = args.get_asset_name.lower()
        assets = api.list_assets()
        for asset in assets:
            if asset.name.lower() == q or asset.symbol.lower() == q or asset.hash.lower() == q:
                if json_output:
                    print(json.dumps(asset.__to_json_value__(), sort_keys=True, indent=2))
                else:
                    print(asset)
                return

    elif args.list_assets:
        assets = api.list_assets()
        if json_output:
            res_obj = [asset.__to_json_value__() for asset in assets]
            print(json.dumps(res_obj, sort_keys=True, indent=2))
        else:
            pprint(assets)

    else:
        parser.print_help()


if __name__ == "__main__":
    main()

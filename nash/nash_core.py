import os
import ctypes
import binascii
import json
import time
from typing import Tuple, Dict
import platform

# Load the the shared nash core library created and exposed by Golang.
# Assumed to be in the current directory (important to keep this for reuse in the Python api client).
platform_sys = platform.system()
if platform_sys == "Linux":
    machine = os.uname()[4]
    if machine.startswith("arm"):
        # ARM - eg. Raspberry Pi
        so_fn = "nash.linux.arm.so"
    else:
        # Standard 64-bit Linux
        so_fn = "nash.linux.so"

elif platform_sys == "Darwin":
    so_fn = "nash.macos.so"

else:
    print("Incompatible OS: %s. Need either Linux (x86_64 or ARM), or OSX" % platform_sys)
    exit(1)


dir_current = os.path.dirname(os.path.realpath(__file__))
so_filename = os.path.join(dir_current, so_fn)
lib = ctypes.cdll.LoadLibrary(so_filename)

# DeriveHKDFKeysFromPassword function signature.
lib.DeriveHKDFKeysFromPassword.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
lib.DeriveHKDFKeysFromPassword.restype = ctypes.c_char_p

# Payload type definitions
PAYLOAD_TYPE_LIST_ACCOUNT_ORDERS = 0
PAYLOAD_TYPE_CANCEL_ORDER = 1
PAYLOAD_TYPE_LIST_ACCOUNT_BALANCE = 2
PAYLOAD_TYPE_LIST_ACCOUNT_VOLUMES = 3
PAYLOAD_TYPE_LIST_MOVEMENTS = 4
PAYLOAD_TYPE_GET_ACCOUNT_BALANCE = 5
PAYLOAD_TYPE_GET_DEPOSIT_ADDRESS = 6
PAYLOAD_TYPE_GET_MOVEMENT = 7
PAYLOAD_TYPE_GET_ACCOUNT_ORDER = 8
PAYLOAD_TYPE_PLACE_LIMIT_ORDER = 9
PAYLOAD_TYPE_PLACE_STOP_LIMIT_ORDER = 10
PAYLOAD_TYPE_PLACE_STOP_MARKET_ORDER = 11
PAYLOAD_TYPE_PLACE_MARKET_ORDER = 12
PAYLOAD_TYPE_ADD_MOVEMENT = 13
PAYLOAD_TYPE_SYNC_STATE = 14
PAYLOAD_TYPE_CANCEL_ALL_ORDERS = 17
PAYLOAD_TYPE_LIST_ACCOUNT_TRANSACTIONS = 18
PAYLOAD_TYPE_GET_ACCOUNT_PORTFOLIO = 19
PAYLOAD_TYPE_GET_STATES = 20
PAYLOAD_TYPE_SIGN_STATES = 21
PAYLOAD_TYPE_UPDATE_MOVEMENT = 22
PAYLOAD_TYPE_LIST_ACCOUNT_STAKES = 23
PAYLOAD_TYPE_LIST_ACCOUNT_STAKING_DIVIDENDS = 24
PAYLOAD_TYPE_LIST_ACCOUNT_STAKING_STATEMENTS = 25
PAYLOAD_TYPE_GET_ORDERS_FOR_MOVEMENT = 26
PAYLOAD_TYPE_GET_ASSET_NONCES = 27

NONCE_EPOCH_START = 155000000000

# Version function signature.
lib.Version.argtypes = []
lib.Version.restype = ctypes.c_char_p


def version() -> str:
    """
    Returns the current version of the go-client.
    """
    return lib.Version().decode("utf-8")


def derive_hkdf_keys_from_password(password: str, salt: str) -> (str, str):
    """
    Derives authentication and encryption keys from a password

    Example:
        password = "super secret"
        salt = "some salt"
        authentication_key, encryption_key = derive_hkdf_keys_from_password(password, salt)

    Returns: authentication_key, encryption_key
    """
    raw = lib.DeriveHKDFKeysFromPassword(
        binascii.hexlify(password.encode("utf8")),
        binascii.hexlify(salt.encode("utf8")),
    )

    res = json.loads(raw)
    return res["authentication_key"], res["encryption_key"]


# Initialize function signature.
lib.Initialize.argtypes = [ctypes.c_char_p]
lib.Initialize.restype = ctypes.c_char_p


def initialize(initParams: Dict) -> Dict:
    """
    Initialize the client by passing in a Dics with initial configuration.

    Example:
        params = {
            "passphrase": password,
            "secretTag": tag,
            "secretNonce": nonce,
            "encryptionKey": encryption_key,
            "secretKey": encrypted_secret_key,
            "enginePubkey": engine_pubkey,
            "chainIndices": {"neo": 1, "eth": 1},
            "assetData": {
                "neo": {
                    "hash": "C56F33FC6ECFCD0C225C4AB356FEE59390AF8560BE0E930FAEBE74A6DAFF7C9B",
                    "precision": 8,
                    "blockchain": "neo"
                },
                "gas": {
                    "hash": "602C79718B16E442DE58778E148D0B1084E3B2DFFD5DE6B7B16CEE7969282DE7",
                    "precision": 8,
                    "blockchain": "neo"
                }
            },
            "marketData": {
                "neo_gas": {
                    "minTickSize": 6
                    "minTradeSize": 0
                },
                "gas_neo": {
                    "minTickSize": 6
                    "minTradeSize": 6
                },
                "neo_eth": {
                    "minTickSize": 6
                    "minTradeSize": 0
                },
                "eth_neo": {
                    "minTickSize": 6
                    "minTradeSize": 0
                }
            }
        }

        config = initialize(params)
    """
    ser_params = json.dumps(initParams)
    raw = lib.Initialize(ctypes.c_char_p(ser_params.encode("utf-8")))

    return json.loads(raw)


# SignPayload function signature.
lib.SignPayload.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
lib.SignPayload.restype = ctypes.c_char_p


def sign_payload(config: Dict, payload: Dict) -> (str, Dict):
    """
    Sign a payload

    Example:
        config = initialize(params)
        list_orders_parasm = create_list_orders_params()
        signature, intennal_payload = sign_payload(config, list_orders_params)

    Returns: signature, payload
    """
    config = json.dumps(config)
    config = ctypes.c_char_p(config.encode("utf-8"))

    payload = json.dumps(payload)
    payload = ctypes.c_char_p(payload.encode("utf-8"))
    raw = lib.SignPayload(config, payload)
    resp = json.loads(raw)

    if 'blockchain_data' in resp.keys():
        return resp["signature"], resp["payload"], resp['blockchain_data']

    return resp["signature"], resp["payload"]


# SignPayload function signature.
lib.SignStateList.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
lib.SignStateList.restype = ctypes.c_char_p


def sign_state_list(config: Dict, state_list: list, recycled_orders: list) -> (str, Dict):
    """
    Sign a list of states

    Example:
        config = initialize(params)
        states = [{'message':'abc','blockchain':'neo'},{'message':'def','blockchain':'eth'}]
        recycled_orders = [{'message':'abc','blockchain':'neo'},{'message':'def','blockchain':'eth'}]
        signature, internal_payload = sign_state_list(config, states)

    Returns: signature, payload
    """
    config = json.dumps(config)
    config = ctypes.c_char_p(config.encode("utf-8"))
    states_dict = {
        "states": state_list,
        "recycled_orders": recycled_orders
    }
    states = json.dumps(states_dict)
    states = ctypes.c_char_p(states.encode("utf-8"))
    raw = lib.SignStateList(config, states)
    resp = json.loads(raw)

    return resp["signature"], resp["payload"]


# NewMasterSeed function signature.
lib.NewMasterSeed.argtypes = [ctypes.c_char_p]
lib.NewMasterSeed.restype = ctypes.c_char_p


def new_master_seed(passphrase: str):
    """
    Creates a new master seed from the given passphrase.

    Example:
        master_seed, mnemonic = new_master_seed("super secret")

    """
    passphrase = ctypes.c_char_p(passphrase.encode("utf-8"))
    raw = lib.NewMasterSeed(passphrase)
    resp = json.loads(raw)

    return resp["seed"], resp["mnemonic"]


# NewMasterSeedEncrypted signature
lib.NewEncryptedSecretKey.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
lib.NewEncryptedSecretKey.restype = ctypes.c_char_p


def new_encryped_secret_key(passphrase: str, encryptionKey: str) -> dict:
    """
    Creates a new secret key that is encrypted and returns
    encrypted key, encryption nonce, and encryption tag

    Example:
        result = new_encryped_secret_key("super secret", "encryptionkeyABCREOU")
        print(result)
        {
             "encryptedKey": "302ad59b31ae96b632d1664907ba657d",
             "tag": "f69c76d147404b7a837f095dffaa378f",
             "nonce": "2781664b3c78208e6d66ff89"
        }
    """
    passphrase = ctypes.c_char_p(passphrase.encode("utf-8"))
    encryptionKey = ctypes.c_char_p(encryptionKey.encode("utf-8"))

    raw = lib.NewEncryptedSecretKey(passphrase, encryptionKey)

    resp = json.loads(raw)

    return resp


def create_get_account_portfolio_params(fiat_symbol: str = None, period: str = None):
    """
    http://api.staging.nash.io/docs/graphql/getaccountportfolioparams.doc.html
    """
    payload = {
        "timestamp": create_timestamp()
    }

    if fiat_symbol is not None:
        payload["fiat_symbol"] = fiat_symbol
    if period is not None:
        payload["period"] = period

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_ACCOUNT_PORTFOLIO,
    }


def create_list_account_orders_params(before: str = None,
                                      buy_or_sell: str = None,
                                      limit: int = None,
                                      market_name: str = None,
                                      range_start: str = None,
                                      range_stop: str = None,
                                      type: str = None,
                                      status: str = None) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/listaccountordersparams.doc.html

    Example:
        list_order_params = create_list_orders_params("eth_neo", ORDER_STATUS_OPEN)
    """
    payload = {
        "timestamp": create_timestamp()
    }
    if before is not None:
        payload["before"] = before
    if buy_or_sell is not None:
        payload["buy_or_sell"] = buy_or_sell
    if limit is not None:
        payload["limit"] = limit
    if market_name is not None:
        payload["market_name"] = market_name
    if range_start is not None:
        payload["range_start"] = range_start
    if range_stop is not None:
        payload["range_stop"] = range_stop
    if type is not None:
        payload["type"] = type
    if status is not None:
        payload["status"] = status

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_ORDERS,
    }


def create_cancel_order_params(order_id: str, market_name: str) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/cancelorderparams.doc.html

    Example:
        cancel_order_params = create_cancel_order_params(1001, "gas_neo")
    """
    payload = {
        "market_name": market_name,
        "order_id": order_id,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_CANCEL_ORDER
    }


def create_cancel_all_orders_params(market_name: str = None) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/rootmutationtype.doc.html

    Example:
        cancel_all_orders_params = create_cancel_all_order_params("eth_neo")
    """
    payload = {
        "timestamp": create_timestamp()
    }
    if market_name is not None:
        payload["market_name"] = market_name

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_CANCEL_ALL_ORDERS
    }


def create_list_account_balance_params(ignore_low_balance: bool) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/listaccountbalancesparams.doc.html

    Example:
        list_account_balance_params = create_list_account_balance_params(false)
    """
    payload = {
        "ignore_low_balance": ignore_low_balance,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_BALANCE
    }


def create_list_account_volumes_params() -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/listaccountvolumesparams.doc.html

    Example:
        list_account_volumes = create_list_account_volumes_params()
    """
    payload = {
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_VOLUMES
    }


def create_list_movements_params(currency: str = None, status: str = None, type: str = None) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/listmovementsparams.doc.html

    Example:
        list_movements_params = create_list_movements_params("neo", "completed", "deposit")
    """
    payload = {
        "timestamp": create_timestamp()
    }

    if currency is not None:
        payload["currency"] = currency
    if status is not None:
        payload["status"] = status
    if type is not None:
        payload["type"] = type

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_MOVEMENTS
    }


def create_list_account_transactions_params(cursor: str = None, fiat_symbol: str = None, limit: int = None) -> Dict:
    payload = {
        "timestamp": create_timestamp()
    }

    if cursor is not None:
        payload["cursor"] = cursor
    if fiat_symbol is not None:
        payload["fiat_symbol"] = fiat_symbol
    if limit is not None:
        payload["limit"] = limit

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_TRANSACTIONS
    }


def create_get_account_balance_params(currency: str) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/getaccountbalanceparams.doc.html

    Example:
        get_account_balance_params = create_get_account_balance_params("eth")
    """
    payload = {
        "currency": currency,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_ACCOUNT_BALANCE
    }


def create_get_deposit_address_params(currency: str) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/getdepositaddressparams.doc.html

    Example:
        get_deposit_address_params = create_get_deposit_address_params("neo")
    """
    payload = {
        "currency": currency,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_DEPOSIT_ADDRESS
    }


def create_get_movement_params(id: int) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/getmovementparams.doc.html

    Example:
        get_movement_params = create_get_movement_params(100)
    """
    payload = {
        "movement_id": id,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_MOVEMENT
    }


def create_get_account_order_params(id: int) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/getaccountorderparams.doc.html

    Example:
        get_account_order_params = create_get_account_order_params(12)

    """
    payload = {
        "order_id": id,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_ACCOUNT_ORDER
    }


def create_place_limit_order_params(allow_taker: bool,
                                    amount: Dict,
                                    buy_or_sell: str,
                                    cancellation_policy: str,
                                    limit_price: Dict,
                                    market_name: str,
                                    nonces_to: [int],
                                    nonces_from: [int],
                                    nonce_order: int = 0,
                                    cancel_at: int = None) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/placelimitorderparams.doc.html

    Example:
        amount = create_currency_amount_params("10000000", "neo")
        limit_price = create_currency_price_params("10000000", "eth", "neo")
        place_limit_order_params = create_place_limit_order_params(
            false,
            amount,
            BUY_ORDER,
            CANCELLATION_POLICY_IN_24_HOURS,
            limit_price,
            "eth_neo",
            nonces_to=[1],
            nonces_from=[1])
    """
    payload = {
        "timestamp": create_timestamp(),
        "allow_taker": allow_taker,
        "amount": amount,
        "buy_or_sell": buy_or_sell,
        "cancellation_policy": cancellation_policy,
        "limit_price": limit_price,
        "market_name": market_name,
        "nonces_to": nonces_to,
        "nonces_from": nonces_from,
        "nonce_order": create_timestamp_32() if nonce_order == 0 else nonce_order,
    }

    if cancel_at is not None:
        payload["cancel_at"] = cancel_at

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_PLACE_LIMIT_ORDER
    }


def create_place_stop_limit_order_params(allow_taker: bool,
                                         amount: Dict,
                                         buy_or_sell: str,
                                         cancellation_policy: str,
                                         limit_price: Dict,
                                         market_name: str,
                                         stop_price: Dict,
                                         nonces_to: [int],
                                         nonces_from: [int],
                                         nonce_order: int = 0,
                                         cancel_at: int = None) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/placestoplimitorderparams.doc.html

     Example:
        amount = create_currency_amount_params("10000000", "neo")
        stop_price = create_currency_price_params("10000000", "eth", "neo")
        place_stop_limit_order_params = create_place_stop_limit_order_params(
            false,
            amount,
            BUY_ORDER,
            CANCELLATION_POLICY_IN_24_HOURS,
            stop_price,
            "eth_neo",
            nonces_to=[1],
            nonces_from=[1])

    """

    payload = {
        "timestamp": create_timestamp(),
        "allow_taker": allow_taker,
        "amount": amount,
        "buy_or_sell": buy_or_sell,
        "cancellation_policy": cancellation_policy,
        "limit_price": limit_price,
        "market_name": market_name,
        "stop_price": stop_price,
        "nonces_to": nonces_to,
        "nonces_from": nonces_from,
        "nonce_order": create_timestamp_32() if nonce_order == 0 else nonce_order,
    }

    if cancel_at is not None:
        payload["cancel_at"] = cancel_at

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_PLACE_STOP_LIMIT_ORDER
    }


def create_place_market_order_params(amount: Dict,
                                     buy_or_sell: str,
                                     market_name: str,
                                     nonces_to: [int],
                                     nonces_from: [int],
                                     nonce_order: int = 0) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/placemarketorderparams.doc.html

    Example:
        amount = create_currency_amount_params("8888", "eth")
        market_order = create_place_market_order_params(amount, SELL_ORDER, "eth_neo", nonces_to=[1], nonces_from=[1])
    """
    payload = {
        "timestamp": create_timestamp(),
        "amount": amount,
        "buy_or_sell": buy_or_sell,
        "market_name": market_name,
        "nonces_to": nonces_to,
        "nonces_from": nonces_from,
        "nonce_order": create_timestamp_32() if nonce_order == 0 else nonce_order,
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_PLACE_MARKET_ORDER
    }


def create_place_stop_market_order_params(amount: Dict,
                                          buy_or_sell: str,
                                          market_name: str,
                                          stop_price: Dict,
                                          nonces_to: [int],
                                          nonces_from: [int],
                                          nonce_order: int = 0) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/placestopmarketorderparams.doc.html

    Example:
        amount = create_currency_amount_params("8888", "eth")
        stop_price = create_currency_price_params("10000000", "eth", "neo")
        market_order = create_place_stop_market_order_params(
            amount, SELL_ORDER, "eth_neo", stop_price, nonces_to=[1], nonces_from=[1])

    """
    payload = {
        "timestamp": create_timestamp(),
        "amount": amount,
        "buy_or_sell": buy_or_sell,
        "market_name": market_name,
        "stop_price": stop_price,
        "nonces_to": nonces_to,
        "nonces_from": nonces_from,
        "nonce_order": create_timestamp_32() if nonce_order == 0 else nonce_order,
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_PLACE_STOP_MARKET_ORDER
    }


def create_add_movement_params(address: str, amount: Dict, movementType: str, nonce: int, recycled_orders=[]) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/signmovementparams.doc.html

    Example:
        amount = create_currency_amount_params("10000000", "neo")
        add_movement_params = create_add_movement_params(address, amount, "deposit")
    """
    payload = {
        "address": address,
        "quantity": amount,
        "type": movementType,
        "nonce": nonce,
        "timestamp": create_timestamp(),
        "recycled_orders": recycled_orders
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_ADD_MOVEMENT
    }


def create_update_movement_params(movementId: str, status: str, transactionHash: str = None, transactionPayload: str = None) -> Dict:
    # MovementID         string         `json:"movement_id"`
    # Status             MovementStatus `json:"status"`
    # TransactionHash    string         `json:"transaction_hash,omitempty"`
    # TransactionPayload string         `json:"transaction_payload,omitempty"`
    """
    http://api.staging.nash.io/docs/graphql/signmovementparams.doc.html

    Example:
        update_movement_params = create_update_movement_params("132","pending","beoo......etc","mytxpayload...etc")
    """
    payload = {
        "movement_id": movementId,
        "status": status,
        "timestamp": create_timestamp()
    }

    if transactionHash:
        payload['transaction_hash'] = transactionHash
    if transactionPayload:
        payload['transaction_payload'] = transactionPayload

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_ADD_MOVEMENT
    }


def create_get_assets_nonces_params(assets: list) -> Dict:
    payload = {
        "assets": assets,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_ASSET_NONCES
    }


def create_get_orders_for_movement_params(unit: str) -> Dict:
    payload = {
        "unit": unit,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_ORDERS_FOR_MOVEMENT
    }


def create_get_states_params() -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/getstateparams.doc.html

    Example:
        get_state_params = create_get_state_params()
    """
    payload = {
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_GET_STATES
    }


def create_sign_states_params(states: list) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/signstatesparams.doc.html

    States should be a list of ClientSignedState like:

        [
            {
                message: "abc",
                signature: "def"
            },etc
        ]


    Example:

        sign_states_params = create_sign_state_params(states)
    """
    payload = {
        "timestamp": create_timestamp(),
        "client_signed_states": states
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_SIGN_STATES
    }


def create_sync_states_params(server_signed_states: list, override_movements: bool = False) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/syncstatesparams.doc.html

    States should be a list of ServerSignedState like:

        [
            {
                message: "abc",
            },etc
        ]


    Example:

        sync_states_params = create_sync_state_params(server_signed_states)
    """
    payload = {
        "override_movements": override_movements,
        "server_signed_states": server_signed_states,
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_SYNC_STATE
    }


def create_list_account_stakes_params() -> Dict:
    payload = {
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_STAKES
    }


def create_list_account_staking_dividends_params(month: int, year: int) -> Dict:
    payload = {
        "timestamp": create_timestamp(),
        "month": month,
        "year": year
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_STAKING_DIVIDENDS
    }


def create_list_account_staking_statements_params() -> Dict:
    payload = {
        "timestamp": create_timestamp()
    }

    return {
        "raw": json.dumps(payload),
        "kind": PAYLOAD_TYPE_LIST_ACCOUNT_STAKING_STATEMENTS
    }


def create_currency_amount_params(amount: str, currency: str) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/currencyamountparams.doc.html

    Example:
        amount = create_currency_amount_params("10000000", "eth")
    """
    return {
        "amount": amount,
        "currency": currency
    }


def create_currency_price_params(amount: str, currency_a: str, currency_b: str) -> Dict:
    """
    http://api.staging.nash.io/docs/graphql/currencypriceparams.doc.html

    Example:
        price_params = create_currency_price_params("10000000", "neo", "eth")
    """
    return {
        "amount": amount,
        "currency_a": currency_a,
        "currency_b": currency_b
    }


def create_timestamp() -> int:
    return int(time.time() * 1000)


def create_timestamp_32() -> int:
    return int(create_timestamp() / 10) - NONCE_EPOCH_START


class CurrencyAmount:
    currency: str
    amount: str

    def __init__(self, amount: str, currency: str):
        self.amount = amount.lower()
        self.currency = currency.lower()

    def ToParams(self) -> dict:
        return {
            "amount": self.amount,
            "currency": self.currency
        }

    @staticmethod
    def FromString(value) -> 'CurrencyAmount':
        try:
            items = value.split(' ')
            return CurrencyAmount(items[0], items[1])
        except Exception as e:
            print("Could not parse currency amount from %s " % value)

    def __str__(self) -> str:
        return "%s %s" % (self.amount, self.currency)

    def __repr__(self) -> str:
        return str(self)


class CurrencyPrice:
    currency_a: str
    currency_b: str
    amount: str

    def __init__(self, amount: str, currency_a: str, currency_b: str):
        self.amount = amount.lower()
        self.currency_a = currency_a.lower()
        self.currency_b = currency_b.lower()

    def ToParams(self) -> dict:
        return {
            "amount": self.amount,
            "currency_a": self.currency_a,
            "currency_b": self.currency_b
        }

    @staticmethod
    def FromString(value) -> 'CurrencyPrice':
        # 1.2 gas per neo
        try:
            items = value.split(' ')
            assert len(items) == 4
            assert items[2].lower() == 'per'
            return CurrencyPrice(items[0], items[1], items[3])
        except Exception as e:
            print("Could not parse currency price from %s. Format is for example '9.2 usdc per neo'" % value)

    def __str__(self) -> str:
        return "%s %s per %s" % (self.amount, self.currency_a, self.currency_b)

    def __repr__(self) -> str:
        return str(self)

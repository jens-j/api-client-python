import os
import logging

# Whether to automatically create a set of keys after login, if this
# account does not yet have keys associated
AUTO_CREATE_KEYS_IF_NOT_EXISTS = True

# Debug outputs
DEBUG_ALL = os.getenv("DEBUG")
DEBUG_GQL_REQUEST = DEBUG_ALL or os.getenv("GQL_PRINT_REQUEST")
DEBUG_GQL_RESPONSE = DEBUG_ALL or os.getenv("GQL_PRINT_RESPONSE")

# Loglevel environment variable (can be 'debug', 'info', 'warning', 'error', 'critical')
_LOGLEVEL_ENV = os.getenv("LOGLEVEL")

# API endpoints
ENV_PROD = "production"
ENV_SANDBOX = "sandbox"
ENV_LOCAL = "local"
ENV_STAGING = "staging"
ENV_BLUEFIELD = "bluefield"
ENV_SUBGAME = "subgame"
ENV_EQUILIBRIUM = 'equilibrium'
ENV_MAIN = "main"  # deprecated. same as production

ENDPOINTS = {
    ENV_SANDBOX: "https://app.sandbox.nash.io/api",
    ENV_LOCAL: "http://localhost:4000/api",
    ENV_STAGING: "https://app.staging.nash.io/api",
    ENV_BLUEFIELD: "https://app.bluefield.nash.io/api",
    ENV_SUBGAME: "https://app.subgame.nash.io/api",
    ENV_EQUILIBRIUM: "https://app.equilibrium.nash.io/api",
    ENV_PROD: "https://app.nash.io/api"
}

# Set the default env
ENV = os.environ.get("NASH_ENV", ENV_SANDBOX)

# If user sets the deprecated "main" env, use prod:
if ENV == ENV_MAIN:
    ENV = ENV_PROD

# Set the hosts based on env (default to live production environment)
assert ENV in ENDPOINTS, "Invalid NASH_ENV value '%s'. Possible: '%s'" % (ENV, "', '".join(ENDPOINTS.keys()))
ENDPOINT = ENDPOINTS[ENV]

# Direct overrides are possible too
if os.environ.get("ENDPOINT"):
    ENDPOINT = os.environ.get("ENDPOINT")

# Default salt used on both local and public envs
SALT = ""

# Check and setup the loglevel
if _LOGLEVEL_ENV and not hasattr(logging, _LOGLEVEL_ENV.upper()):
    raise Exception("Loglevel '%s' does not exist" % _LOGLEVEL_ENV)
_LOGLEVEL_IF_NOT_SET_BY_ENV = logging.DEBUG if DEBUG_ALL else logging.INFO
LOGLEVEL_DEFAULT = getattr(logging, _LOGLEVEL_ENV.upper()) if _LOGLEVEL_ENV else _LOGLEVEL_IF_NOT_SET_BY_ENV

GO_CRYPTO_CORE_VERSION_EXPECTED = "0.01"

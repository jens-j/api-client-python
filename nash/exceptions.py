class LoginFailedError(Exception):
    """ Is raised whenever any error happens on login """
    pass


class VerificationNeededError(Exception):
    """ Is raised when account trying to login has not been verified """
    pass


class CreateAccountFailedError(Exception):
    """ Is raised whenever creating an account encounters an error """
    pass


class UploadKeysFailedError(Exception):
    """ Is raised whenever upload keys fails"""
    pass


class WalletKeysNotFoundError(Exception):
    """ Is raised when login is successful but account has not provided wallet keys """
    pass


class GraphQlError(Exception):
    """ Is raised when the GraphQL request returned an error """
    pass


class MaintenanceModeError(Exception):
    """ Is raised when Nash is in maintenance mode """
    pass

import requests
from nash.logger import logger

from nash import settings
from nash.exceptions import LoginFailedError, UploadKeysFailedError, VerificationNeededError, MaintenanceModeError
from nash.graphql_schema import graphql_schema as schema
from nash.graphql_schema import Market
from nash.nash_core import CurrencyAmount

session = requests.Session()


def login(email, login_password):
    """
    Helper for login.
    Returns: cas_api_cookie, response
    Raises: nash.exceptions.LoginFailedError
    """
    # Do the CAS API login
    url = settings.ENDPOINT + "/user_login"
    logger.debug("login at %s with user=%s and authkey=%s", url, email, login_password)
    body = {
        "email": email,
        "password": login_password
    }

    headers = {"Content-Type": "application/json"}

    try:
        result = session.post(url, json=body, headers=headers)
    except Exception as e:
        print("Could not log in : %s " % e)
        raise LoginFailedError(str(e))

    if result.status_code != 200:
        if "verify your account" in result.text:
            msg = "Account %s needs verification " % email
            logger.warn(msg)
            raise VerificationNeededError(msg)
        err = "%s: %s" % (result.status_code, result.text)
        logger.error(err)
        if result.status_code == 503:
            raise MaintenanceModeError("Nash is currently in maintenance mode. Please try again later.")
        raise LoginFailedError(err)

    response = result.json()

    # doesn't seem like api token is there, but set-cookie is
    cas_api_cookie = result.headers.get("set-cookie")
    logger.debug("logged in as user %s at %s" % (email, url))
    return cas_api_cookie, response


def upload_wallet_and_keys(core_params: dict, encrypted_key_data: dict, api_cookie: str) -> bool:
    """
    Helper for updloading wallet/keys.
    Returns: bool
    Raises: nash.exceptions.UploadKeysFailedError

    expects something like the following
    {
      "signature_public_key": "024b14170f0166ff85882356295f5aa0cf4a9a5d29725b5a9e410ec193d20ee98f",
      "encrypted_secret_key": "eb13bb0e89102d64700906c7082f9472",
      "encrypted_secret_key_nonce": "f6783fe349320f71acc2ca79",
      "encrypted_secret_key_tag": "7c8dc1020de77cd42dbbbb850f4335e8",
      "wallets": [
        {
          "blockchain": "neo",
          "address": "Aet6eGnQMvZ2xozG3A3SvWrMFdWMvZj1cU",
          "public_key": "039fcee26c1f54024d19c0affcf6be8187467c9ba4749106a4b897a08b9e8fed23"
        },
        {
          "blockchain": "ethereum",
          "address": "5f8b6d9d487c8136cc1ad87d6e176742af625de8",
          "public_key": "04d37f1a8612353ffbf20b0a68263b7aae235bd3af8d60877ed8135c27630d895894885f220a39acab4e70b025b1aca95fab1cd9368bf3dc912ef32dc65aecfa02"
        }
      ]
    }
    """

    url = settings.ENDPOINT + "/auth/add_initial_wallets_and_client_keys"
    logger.debug("Uploading wallet and keys")

    body = {
        "signature_public_key": core_params["PayloadSigning"]["PublicKey"],
        "encrypted_secret_key": encrypted_key_data["encryptedKey"],
        "encrypted_secret_key_nonce": encrypted_key_data["nonce"],
        "encrypted_secret_key_tag": encrypted_key_data["tag"],
        "wallets": [
            {
                "blockchain": "eth",
                "address": core_params["Wallets"]["eth"]["Address"].upper(),
                "public_key": core_params["Wallets"]["eth"]["PublicKey"]
            },
            {
                "blockchain": "neo",
                "address": core_params["Wallets"]["neo"]["Address"],
                "public_key": core_params["Wallets"]["neo"]["PublicKey"]
            },
            # {
            #     "blockchain": "btc",
            #     "address": core_params["Wallets"]["btc"]["Address"],
            #     "public_key": core_params["Wallets"]["btc"]["PublicKey"]
            # },
        ]

    }

    headers = {
        "Content-Type": "application/json",
        "Cookie": api_cookie
    }

    try:
        result = session.post(url, json=body, headers=headers)
    except Exception as e:
        raise UploadKeysFailedError(str(e))

    if result.status_code != 200:
        err = "%s: %s" % (result.status_code, result.text)
        logger.error(err)
        if result.status_code == 503:
            raise MaintenanceModeError("Nash is currently in maintenance mode. Please try again later.")
        raise UploadKeysFailedError(err)


def verify_market_name(market_name: str, markets: dict, to_lower_case=True):
    """verifies a market name is valid

    Args:
        market_name (str): market name value to check
        markets (dict): dictionary of current markets
        to_lower_case (bool, optional): whether to lowercase market name. Defaults to True.

    Raises:
        AttributeError: No market name supplied
        AttributeError: Invalid market name
    """
    if not market_name:
        raise AttributeError("No market_name supplied. Possible: %s" % (", ".join(list(markets.keys()))))
    if to_lower_case:
        market_name = market_name.lower()
    if market_name not in markets:
        raise AttributeError("Invalid market_name '%s'. Possible: %s" % (market_name, ", ".join(list(markets.keys()))))


def verify_and_normalize_market_name(market_name: str, markets: dict, to_lower_case=True) -> str:
    """Verifies and normalizes a market name

    Args:
        market_name (str): market name value to check
        markets (dict): dictionary of current markets
        to_lower_case (bool, optional): whether to lowercase market name. Defaults to True.

    Returns:
        str: Verified market name
    """
    verify_market_name(market_name, markets, to_lower_case)
    return market_name.lower() if to_lower_case else market_name


def verify_buy_or_sell(buy_or_sell: str, to_upper_case=True):
    """verifies whether a value is a valid buy or sell str

    Args:
        buy_or_sell (str): value to verify
        to_upper_case (bool, optional): whether to lowercase the value. Defaults to True.

    Raises:
        AttributeError: No value specified
        AttributeError: Invalid value specified
    """
    if not buy_or_sell:
        raise AttributeError("No buy_or_sell argument supplied. Possible: %s" % (", ".join(schema.OrderBuyOrSell.__choices__)))
    if to_upper_case:
        buy_or_sell = buy_or_sell.upper()
    if buy_or_sell not in schema.OrderBuyOrSell.__choices__:
        raise AttributeError("Invalid buy_or_sell argument '%s'. Possible: %s" % (buy_or_sell, ", ".join(schema.OrderBuyOrSell.__choices__)))


def verify_and_normalize_buy_or_sell(buy_or_sell, to_upper_case=True) -> str:
    """Verifies and normalizes buy or sell value

    Args:
        buy_or_sell (str): value to verify
        to_upper_case (bool, optional): whether to lowercase the value. Defaults to True.

    Returns:
        str: verified buy or sell value
    """
    verify_buy_or_sell(buy_or_sell, to_upper_case)
    return buy_or_sell.upper() if to_upper_case else buy_or_sell


def verify_cancellation_policy(cancellation_policy, to_upper_case=True):
    if not cancellation_policy:
        raise AttributeError("No cancellation_policy argument supplied. Possible: %s" % (", ".join(schema.OrderCancellationPolicy.__choices__)))
    if to_upper_case:
        cancellation_policy = cancellation_policy.upper()
    if cancellation_policy not in schema.OrderCancellationPolicy.__choices__:
        raise AttributeError("Invalid cancellation_policy argument '%s'. Possible: %s" % (cancellation_policy, ", ".join(schema.OrderCancellationPolicy.__choices__)))


def verify_and_normalize_cancellation_policy(cancellation_policy, to_upper_case=True):
    verify_cancellation_policy(cancellation_policy, to_upper_case)
    return cancellation_policy.upper() if to_upper_case else cancellation_policy


def market_precision_to_num_decimals(market_min_tick_size) -> int:
    """
    Takes scientific notation string for min_tick_size and min_trade_size (e.g. "1e-06")
    and returns the number of decimals needed for posting an order. Can also be in other
    formats from the backend, like 1.0 (precision 1), or 0.01 (precision 2).
    """
    assert isinstance(market_min_tick_size, str)
    if "e-" in market_min_tick_size:
        return int(market_min_tick_size.split("-")[1])
    elif float(market_min_tick_size) == 0.0:
        return 0
    elif "." in market_min_tick_size:
        a, b = market_min_tick_size.split(".")
        return len(b)
    elif int(market_min_tick_size) > -1:
        return int(market_min_tick_size)
    raise Exception("Invalid market_min_tick_size: %s", market_min_tick_size)


def normalize_price_for_market_precision(amount: str, market: Market) -> str:

    required_decimals = market_precision_to_num_decimals(market.min_trade_increment_b)

    return normalize_to_required_decimals(amount, required_decimals)


def normalize_amount_for_market_precision(amount: CurrencyAmount, market: Market) -> CurrencyAmount:

    required_decimals = market_precision_to_num_decimals(market.min_trade_increment)

    if amount.currency == market.b_unit:
        required_decimals = market_precision_to_num_decimals(market.min_trade_increment_b)

    amount.amount = normalize_to_required_decimals(amount.amount, required_decimals)
    return amount


def normalize_to_required_decimals(amount: str, required_decimals: int) -> str:
    _amount = amount
    amount_split = _amount.split(".")

    # Case 1: No decimals
    if required_decimals == 0:
        if len(amount_split) == 1:
            return _amount
        raise AttributeError("Too many decimals (%s). Maximum decimals allowed are %s." % (len(amount_split[1]), required_decimals))

    # Case 2: >0 decimals
    if len(amount_split) == 1:
        # No user supplied decimals are easy: just add 0s
        _amount += "." + "0" * required_decimals
    elif len(amount_split[1]) < required_decimals:
        # Less decimals are easy: fill with 0s
        _amount += "0" * (required_decimals - len(amount_split[1]))
    elif len(amount_split[1]) > required_decimals:
        # more decimals can only be processed if 0s, simply truncate
        if int(amount_split[1][required_decimals:]) == 0:
            amount_split[1] = amount_split[1][:required_decimals]
            _amount = ".".join(amount_split)
        else:
            raise AttributeError("Too many decimals (%s). Maximum decimals allowed are %s." % (len(amount_split[1]), required_decimals))
    return _amount


def _test_market_precision_to_num_decimals():
    assert market_precision_to_num_decimals("0") == 0
    assert market_precision_to_num_decimals("0.0") == 0
    assert market_precision_to_num_decimals("1.0") == 1
    assert market_precision_to_num_decimals("0.1") == 1
    assert market_precision_to_num_decimals("0.01") == 2
    assert market_precision_to_num_decimals("1.0e-2") == 2


def _test_normalize_amount():
    precision_str = "1.0e-06"
    assert normalize_amount_for_market_precision("10", precision_str) == "10.000000"
    assert normalize_amount_for_market_precision("10.0", precision_str) == "10.000000"
    assert normalize_amount_for_market_precision("10.1", precision_str) == "10.100000"
    assert normalize_amount_for_market_precision("10.123456", precision_str) == "10.123456"
    assert normalize_amount_for_market_precision("10.1234560000", precision_str) == "10.123456"

    exception_raised = False
    try:
        normalize_amount_for_market_precision("10.12345678", precision_str)
    except Exception:
        # Exception is expected because more decimals than can be processed
        exception_raised = True
    assert exception_raised


# _test_normalize_amount()
# _test_market_precision_to_num_decimals()

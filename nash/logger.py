"""
This logger is simply reusing logzero and setting some defaults.

See also https://logzero.readthedocs.io/
"""
from nash import settings
from logzero import logger, loglevel  # provides all the logzero methods to nash.logger

assert logger
loglevel(settings.LOGLEVEL_DEFAULT)

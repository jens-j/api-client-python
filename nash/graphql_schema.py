import sgqlc.types
import sgqlc.types.datetime


graphql_schema = sgqlc.types.Schema()


########################################################################
# Scalars and Enumerations
########################################################################
class Base16(sgqlc.types.Scalar):
    __schema__ = graphql_schema


class Blockchain(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('ETH', 'NEO')


class BlockchainName(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BTC', 'ETH', 'NEO')


Boolean = sgqlc.types.Boolean


class CandleInterval(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('FIFTEEN_MINUTE', 'ONE_DAY', 'ONE_HOUR', 'ONE_MINUTE', 'ONE_MONTH', 'ONE_WEEK', 'SIX_HOUR', 'THIRTY_MINUTE', 'TWELVE_HOUR')


class CountryStatusEnum(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('ALLOWED', 'BLOCKED', 'RESTRICTED')


class CurrencyNumber(sgqlc.types.Scalar):
    __schema__ = graphql_schema


class CurrencySymbol(sgqlc.types.Scalar):
    __schema__ = graphql_schema


Date = sgqlc.types.datetime.Date

DateTime = sgqlc.types.datetime.DateTime


class Direction(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BUY', 'SELL')


Float = sgqlc.types.Float

ID = sgqlc.types.ID

Int = sgqlc.types.Int


class Json(sgqlc.types.Scalar):
    __schema__ = graphql_schema


class KycErrorDetailsEnum(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BLACK_AND_WHITE', 'IDENTITY_VERIFICATION_TOO_MANY_ATTEMPTS', 'MANUAL_REJECTION', 'LIVENESS_FAILED', 'IP_ADDRESS_IS_PROXY', 'SAMPLE_DOCUMENT', 'DAMAGED_DOCUMENT', 'PHOTOCOPY_BLACK_WHITE', 'UNKNOWN_ERROR', 'NO_DOCUMENT', 'NONE', 'IP_ADDRESS_AND_ADDRESS_MISMATCH', 'NOT_READABLE_DOCUMENT', 'MULTIPLE_PEOPLE', 'NO_MATCH', 'LOCATION_BANNED', 'MISSING_PART_DOCUMENT', 'HIDDEN_PART_DOCUMENT', 'DIGITAL_COPY', 'NO_FACE_PRESENT', 'MISSING_SIGNATURE', 'CAMERA_BLACK_WHITE', 'IP_ADDRESS_IS_DANGEROUS', 'UNDERAGE', 'BIRTHDATE_INVALID_ON_ID', 'IP_ADDRESS_IS_CRAWLER', 'IP_ADDRESS_IS_TOR', 'FACE_NOT_FULLY_VISIBLE', 'MISSING_BACK', 'BAD_QUALITY', 'FOUND_SCREENING_RESULTS', 'BLURRED', 'DIFFERENT_PERSONS_SHOWN', 'DUPLICATE_ACCOUNT', 'WRONG_DOCUMENT_PAGE', 'PHOTOCOPY_COLOR', 'DENIED_FRAUD')


class KycStatusEnum(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('APPROVED_AGE_VERIFICATION', 'APPROVED_LOCATION_VERIFICATION', 'APPROVED_SANCTION_SCREENING', 'BANNED', 'FAILED_IDENTITY_VERIFICATION', 'FAILED_LOCATION_VERIFICATION', 'NO_LIMIT', 'REJECTED', 'SUBMITTED_AGE_LOCATION', 'TIER_0', 'TIER_1', 'TIER_2', 'UPLOADED_IDENTITY_VERIFICATION')


class MarketName(sgqlc.types.Scalar):
    __schema__ = graphql_schema


class MarketStatus(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('INITIALIZING', 'OFFLINE', 'PAUSED', 'RUNNING', 'SHUTTING_DOWN')


class MetricPeriod(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('DAY', 'HOUR', 'MONTH', 'WEEK')


class MovementStatus(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('COMPLETED', 'CREATED', 'FAILED', 'PENDING')


class MovementType(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('DEPOSIT', 'TRANSFER', 'WITHDRAWAL')


class OrderBuyOrSell(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BUY', 'SELL')


class OrderCancellationPolicy(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('FILL_OR_KILL', 'GOOD_TIL_CANCELLED', 'GOOD_TIL_TIME', 'IMMEDIATE_OR_CANCEL')


class OrderCancellationReason(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('ADMIN_CANCELLED', 'EXPIRATION', 'INVALID_FOR_ORDERBOOK_STATE', 'NO_FILL', 'USER')


class OrderStatus(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('CANCELLED', 'FILLED', 'OPEN', 'PENDING')


class OrderType(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('LIMIT', 'MARKET', 'STOP_LIMIT', 'STOP_MARKET')


class PaginationCursor(sgqlc.types.Scalar):
    __schema__ = graphql_schema


class PortfolioGraphPeriod(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('DAY', 'HOUR', 'MONTH', 'SEMESTER', 'WEEK')


class RestrictionEnum(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BANNED', 'EXCHANGE', 'STAKING')


class StakeStatus(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('ACTIVE', 'COMPLETE')


String = sgqlc.types.String


class VerificationLevelEnum(sgqlc.types.Enum):
    __schema__ = graphql_schema
    __choices__ = ('BANNED', 'NO_LIMIT', 'TIER_0', 'TIER_1', 'TIER_2')


########################################################################
# Input Objects
########################################################################
class AddMovementParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='address')
    nonce = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonce')
    quantity = sgqlc.types.Field(sgqlc.types.non_null('CurrencyAmountParams'), graphql_name='quantity')
    resigned_orders = sgqlc.types.Field(sgqlc.types.list_of('ClientSignedMessage'), graphql_name='resignedOrders')
    target_address = sgqlc.types.Field(String, graphql_name='targetAddress')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    transaction_hash = sgqlc.types.Field(Base16, graphql_name='transactionHash')
    transaction_payload = sgqlc.types.Field(Base16, graphql_name='transactionPayload')
    type = sgqlc.types.Field(sgqlc.types.non_null(MovementType), graphql_name='type')


class BlockchainSignature(sgqlc.types.Input):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(sgqlc.types.non_null(Blockchain), graphql_name='blockchain')
    nonce_from = sgqlc.types.Field(Int, graphql_name='nonceFrom')
    nonce_to = sgqlc.types.Field(Int, graphql_name='nonceTo')
    public_key = sgqlc.types.Field(Base16, graphql_name='publicKey')
    signature = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='signature')


class CancelAllOrdersParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    market_name = sgqlc.types.Field(MarketName, graphql_name='marketName')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class CancelOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    market_name = sgqlc.types.Field(sgqlc.types.non_null(MarketName), graphql_name='marketName')
    order_id = sgqlc.types.Field(sgqlc.types.non_null(ID), graphql_name='orderId')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class ClientSignedMessage(sgqlc.types.Input):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(Blockchain, graphql_name='blockchain')
    message = sgqlc.types.Field(Base16, graphql_name='message')
    signature = sgqlc.types.Field(Base16, graphql_name='signature')


class CurrencyAmountParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyNumber), graphql_name='amount')
    currency = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='currency')


class CurrencyPriceParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyNumber), graphql_name='amount')
    currency_a = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='currencyA')
    currency_b = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='currencyB')


class FiatGatewayCredentialInput(sgqlc.types.Input):
    __schema__ = graphql_schema
    partner_account_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerAccountId')
    partner_account_key = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerAccountKey')
    partner_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerId')


class GetAccountBalanceParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    currency = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='currency')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetAccountOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    order_id = sgqlc.types.Field(sgqlc.types.non_null(ID), graphql_name='orderId')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetAccountPortfolioParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    fiat_symbol = sgqlc.types.Field(String, graphql_name='fiatSymbol')
    ignore_low_balance = sgqlc.types.Field(Boolean, graphql_name='ignoreLowBalance')
    period = sgqlc.types.Field(PortfolioGraphPeriod, graphql_name='period')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetAccountVolumesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetAssetsNoncesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    assets = sgqlc.types.Field(sgqlc.types.list_of(CurrencySymbol), graphql_name='assets')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetDepositAddressParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    currency = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='currency')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetMovementParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    atomic = sgqlc.types.Field(Boolean, graphql_name='atomic')
    movement_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='movementId')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class GetOrdersForMovementParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    unit = sgqlc.types.Field(CurrencySymbol, graphql_name='unit')


class GetStatesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class InputServerSignedState(sgqlc.types.Input):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(sgqlc.types.non_null(Blockchain), graphql_name='blockchain')
    message = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='message')


class ListAccountBalancesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    ignore_low_balance = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='ignoreLowBalance')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class ListAccountOrdersParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    before = sgqlc.types.Field(PaginationCursor, graphql_name='before')
    buy_or_sell = sgqlc.types.Field(OrderBuyOrSell, graphql_name='buyOrSell')
    limit = sgqlc.types.Field(Int, graphql_name='limit')
    market_name = sgqlc.types.Field(MarketName, graphql_name='marketName')
    range_start = sgqlc.types.Field(DateTime, graphql_name='rangeStart')
    range_stop = sgqlc.types.Field(DateTime, graphql_name='rangeStop')
    status = sgqlc.types.Field(sgqlc.types.list_of(OrderStatus), graphql_name='status')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    type = sgqlc.types.Field(sgqlc.types.list_of(OrderType), graphql_name='type')


class ListAccountStakesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class ListAccountStakingDividendsParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    month = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='month')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    year = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='year')


class ListAccountStakingStatementsParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class ListAccountTransactionsParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    cursor = sgqlc.types.Field(PaginationCursor, graphql_name='cursor')
    fiat_symbol = sgqlc.types.Field(String, graphql_name='fiatSymbol')
    limit = sgqlc.types.Field(Int, graphql_name='limit')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class ListMovementsParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    atomic = sgqlc.types.Field(Boolean, graphql_name='atomic')
    currency = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')
    status = sgqlc.types.Field(MovementStatus, graphql_name='status')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    type = sgqlc.types.Field(MovementType, graphql_name='type')


class PaginationOptions(sgqlc.types.Input):
    __schema__ = graphql_schema
    after = sgqlc.types.Field(String, graphql_name='after')
    before = sgqlc.types.Field(String, graphql_name='before')
    limit = sgqlc.types.Field(Int, graphql_name='limit')


class PlaceLimitOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    allow_taker = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='allowTaker')
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyAmountParams), graphql_name='amount')
    blockchain_signatures = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(BlockchainSignature)), graphql_name='blockchainSignatures')
    buy_or_sell = sgqlc.types.Field(sgqlc.types.non_null(OrderBuyOrSell), graphql_name='buyOrSell')
    cancel_at = sgqlc.types.Field(DateTime, graphql_name='cancelAt')
    cancellation_policy = sgqlc.types.Field(sgqlc.types.non_null(OrderCancellationPolicy), graphql_name='cancellationPolicy')
    limit_price = sgqlc.types.Field(sgqlc.types.non_null(CurrencyPriceParams), graphql_name='limitPrice')
    market_name = sgqlc.types.Field(sgqlc.types.non_null(MarketName), graphql_name='marketName')
    nonce_from = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceFrom')
    nonce_order = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceOrder')
    nonce_to = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceTo')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class PlaceMarketOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyAmountParams), graphql_name='amount')
    blockchain_signatures = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(BlockchainSignature)), graphql_name='blockchainSignatures')
    buy_or_sell = sgqlc.types.Field(sgqlc.types.non_null(OrderBuyOrSell), graphql_name='buyOrSell')
    market_name = sgqlc.types.Field(sgqlc.types.non_null(MarketName), graphql_name='marketName')
    nonce_from = sgqlc.types.Field(Int, graphql_name='nonceFrom')
    nonce_order = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceOrder')
    nonce_to = sgqlc.types.Field(Int, graphql_name='nonceTo')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class PlaceStopLimitOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    allow_taker = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='allowTaker')
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyAmountParams), graphql_name='amount')
    blockchain_signatures = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(BlockchainSignature)), graphql_name='blockchainSignatures')
    buy_or_sell = sgqlc.types.Field(sgqlc.types.non_null(OrderBuyOrSell), graphql_name='buyOrSell')
    cancel_at = sgqlc.types.Field(DateTime, graphql_name='cancelAt')
    cancellation_policy = sgqlc.types.Field(sgqlc.types.non_null(OrderCancellationPolicy), graphql_name='cancellationPolicy')
    limit_price = sgqlc.types.Field(sgqlc.types.non_null(CurrencyPriceParams), graphql_name='limitPrice')
    market_name = sgqlc.types.Field(sgqlc.types.non_null(MarketName), graphql_name='marketName')
    nonce_from = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceFrom')
    nonce_order = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceOrder')
    nonce_to = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceTo')
    stop_price = sgqlc.types.Field(sgqlc.types.non_null(CurrencyPriceParams), graphql_name='stopPrice')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class PlaceStopMarketOrderParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(sgqlc.types.non_null(CurrencyAmountParams), graphql_name='amount')
    blockchain_signatures = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(BlockchainSignature)), graphql_name='blockchainSignatures')
    buy_or_sell = sgqlc.types.Field(sgqlc.types.non_null(OrderBuyOrSell), graphql_name='buyOrSell')
    market_name = sgqlc.types.Field(sgqlc.types.non_null(MarketName), graphql_name='marketName')
    nonce_from = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceFrom')
    nonce_order = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceOrder')
    nonce_to = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='nonceTo')
    stop_price = sgqlc.types.Field(sgqlc.types.non_null(CurrencyPriceParams), graphql_name='stopPrice')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class SignStatesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    client_signed_states = sgqlc.types.Field(sgqlc.types.list_of(ClientSignedMessage), graphql_name='clientSignedStates')
    signed_recycled_orders = sgqlc.types.Field(sgqlc.types.list_of(ClientSignedMessage), graphql_name='signedRecycledOrders')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class SignUpIdentity(sgqlc.types.Input):
    __schema__ = graphql_schema
    country_code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='countryCode')
    full_name = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='fullName')
    state_code = sgqlc.types.Field(String, graphql_name='stateCode')


class SignUpInput(sgqlc.types.Input):
    __schema__ = graphql_schema
    email = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='email')
    identity = sgqlc.types.Field(sgqlc.types.non_null(SignUpIdentity), graphql_name='identity')
    password = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='password')


class Signature(sgqlc.types.Input):
    __schema__ = graphql_schema
    public_key = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='publicKey')
    signed_digest = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='signedDigest')


class SyncStatesParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    override_movements = sgqlc.types.Field(Boolean, graphql_name='overrideMovements')
    server_signed_states = sgqlc.types.Field(sgqlc.types.list_of(InputServerSignedState), graphql_name='serverSignedStates')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')


class UpdateMovementParams(sgqlc.types.Input):
    __schema__ = graphql_schema
    fee = sgqlc.types.Field(String, graphql_name='fee')
    movement_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='movementId')
    status = sgqlc.types.Field(MovementStatus, graphql_name='status')
    timestamp = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='timestamp')
    transaction_hash = sgqlc.types.Field(Base16, graphql_name='transactionHash')
    transaction_payload = sgqlc.types.Field(Base16, graphql_name='transactionPayload')


class VerificationOptions(sgqlc.types.Input):
    __schema__ = graphql_schema
    encrypted_secret_key = sgqlc.types.Field(String, graphql_name='encryptedSecretKey')
    encrypted_secret_key_nonce = sgqlc.types.Field(String, graphql_name='encryptedSecretKeyNonce')
    encrypted_secret_key_tag = sgqlc.types.Field(String, graphql_name='encryptedSecretKeyTag')
    new_password = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='newPassword')
    signature_public_key = sgqlc.types.Field(Base16, graphql_name='signaturePublicKey')
    two_fa = sgqlc.types.Field(String, graphql_name='twoFa')


class WalletInput(sgqlc.types.Input):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='address')
    blockchain = sgqlc.types.Field(sgqlc.types.non_null(BlockchainName), graphql_name='blockchain')
    chain_index = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='chainIndex')
    public_key = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='publicKey')


########################################################################
# Output Objects and Interfaces
########################################################################
class AccountBalance(sgqlc.types.Type):
    __schema__ = graphql_schema
    asset = sgqlc.types.Field('Asset', graphql_name='asset')
    available = sgqlc.types.Field('CurrencyAmount', graphql_name='available')
    deposit_address = sgqlc.types.Field(String, graphql_name='depositAddress')
    in_orders = sgqlc.types.Field('CurrencyAmount', graphql_name='inOrders')
    pending = sgqlc.types.Field('CurrencyAmount', graphql_name='pending')
    personal = sgqlc.types.Field('CurrencyAmount', graphql_name='personal')


class AccountDepositAddress(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(String, graphql_name='address')
    currency = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')


class AccountKeysAndWalletsResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    creating_account = sgqlc.types.Field(Boolean, graphql_name='creatingAccount')
    email = sgqlc.types.Field(String, graphql_name='email')
    encrypted_secret_key = sgqlc.types.Field(String, graphql_name='encryptedSecretKey')
    encrypted_secret_key_nonce = sgqlc.types.Field(String, graphql_name='encryptedSecretKeyNonce')
    encrypted_secret_key_tag = sgqlc.types.Field(String, graphql_name='encryptedSecretKeyTag')
    id = sgqlc.types.Field(ID, graphql_name='id')
    identity = sgqlc.types.Field('IdentityResponse', graphql_name='identity')
    login_error_count = sgqlc.types.Field(Int, graphql_name='loginErrorCount')
    options = sgqlc.types.Field('Options', graphql_name='options')
    settings = sgqlc.types.Field(Json, graphql_name='settings')
    two_factor = sgqlc.types.Field(Boolean, graphql_name='twoFactor')
    two_factor_error_count = sgqlc.types.Field(String, graphql_name='twoFactorErrorCount')
    verified = sgqlc.types.Field(Boolean, graphql_name='verified')
    wallets = sgqlc.types.Field(sgqlc.types.list_of('Wallet'), graphql_name='wallets')


class AccountPortfolio(sgqlc.types.Type):
    __schema__ = graphql_schema
    balances = sgqlc.types.Field(sgqlc.types.list_of('AccountPortfolioBalance'), graphql_name='balances')
    graph = sgqlc.types.Field(sgqlc.types.list_of('GraphPoint'), graphql_name='graph')
    total = sgqlc.types.Field('AccountPortfolioTotal', graphql_name='total')
    total_staked_tokens = sgqlc.types.Field('CurrencyAmount', graphql_name='totalStakedTokens')


class AccountPortfolioBalance(sgqlc.types.Type):
    __schema__ = graphql_schema
    allocation = sgqlc.types.Field(Float, graphql_name='allocation')
    asset = sgqlc.types.Field('Asset', graphql_name='asset')
    fiat_price = sgqlc.types.Field(Float, graphql_name='fiatPrice')
    fiat_price_change = sgqlc.types.Field(Float, graphql_name='fiatPriceChange')
    fiat_price_change_percent = sgqlc.types.Field(Float, graphql_name='fiatPriceChangePercent')
    total = sgqlc.types.Field(Float, graphql_name='total')
    total_fiat_price = sgqlc.types.Field(Float, graphql_name='totalFiatPrice')
    total_fiat_price_change = sgqlc.types.Field(Float, graphql_name='totalFiatPriceChange')


class AccountPortfolioTotal(sgqlc.types.Type):
    __schema__ = graphql_schema
    available_allocation = sgqlc.types.Field(Float, graphql_name='availableAllocation')
    available_fiat_price = sgqlc.types.Field(Float, graphql_name='availableFiatPrice')
    in_orders_allocation = sgqlc.types.Field(Float, graphql_name='inOrdersAllocation')
    in_orders_fiat_price = sgqlc.types.Field(Float, graphql_name='inOrdersFiatPrice')
    in_stakes_allocation = sgqlc.types.Field(Float, graphql_name='inStakesAllocation')
    in_stakes_fiat_price = sgqlc.types.Field(Float, graphql_name='inStakesFiatPrice')
    pending_allocation = sgqlc.types.Field(Float, graphql_name='pendingAllocation')
    pending_fiat_price = sgqlc.types.Field(Float, graphql_name='pendingFiatPrice')
    personal_allocation = sgqlc.types.Field(Float, graphql_name='personalAllocation')
    personal_fiat_price = sgqlc.types.Field(Float, graphql_name='personalFiatPrice')
    total_fiat_price = sgqlc.types.Field(Float, graphql_name='totalFiatPrice')
    total_fiat_price_change = sgqlc.types.Field(Float, graphql_name='totalFiatPriceChange')
    total_fiat_price_change_percent = sgqlc.types.Field(Float, graphql_name='totalFiatPriceChangePercent')


class AccountResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    creating_account = sgqlc.types.Field(Boolean, graphql_name='creatingAccount')
    email = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='email')
    id = sgqlc.types.Field(ID, graphql_name='id')
    identity = sgqlc.types.Field('IdentityResponse', graphql_name='identity')
    is_duplicate = sgqlc.types.Field(Boolean, graphql_name='isDuplicate')
    options = sgqlc.types.Field('Options', graphql_name='options')
    settings = sgqlc.types.Field(Json, graphql_name='settings')
    two_factor = sgqlc.types.Field(Boolean, graphql_name='twoFactor')
    verified = sgqlc.types.Field(Boolean, graphql_name='verified')


class AccountStake(sgqlc.types.Type):
    __schema__ = graphql_schema
    estimated_next_payment = sgqlc.types.Field(DateTime, graphql_name='estimatedNextPayment')
    id = sgqlc.types.Field(ID, graphql_name='id')
    return_rate = sgqlc.types.Field(Float, graphql_name='returnRate')
    staked_tokens = sgqlc.types.Field('CurrencyAmount', graphql_name='stakedTokens')
    start = sgqlc.types.Field(Date, graphql_name='start')
    status = sgqlc.types.Field(StakeStatus, graphql_name='status')
    stop = sgqlc.types.Field(Date, graphql_name='stop')
    total_dividends = sgqlc.types.Field('CurrencyAmount', graphql_name='totalDividends')


class AccountStakes(sgqlc.types.Type):
    __schema__ = graphql_schema
    stakes = sgqlc.types.Field(sgqlc.types.list_of(AccountStake), graphql_name='stakes')
    total_dividends = sgqlc.types.Field('CurrencyAmount', graphql_name='totalDividends')
    total_staked_tokens = sgqlc.types.Field('CurrencyAmount', graphql_name='totalStakedTokens')


class AccountStakingDividend(sgqlc.types.Type):
    __schema__ = graphql_schema
    asset = sgqlc.types.Field('Asset', graphql_name='asset')
    asset_name = sgqlc.types.Field(String, graphql_name='assetName')
    dividend = sgqlc.types.Field('CurrencyAmount', graphql_name='dividend')
    id = sgqlc.types.Field(ID, graphql_name='id')
    paid_at = sgqlc.types.Field(DateTime, graphql_name='paidAt')
    usd_dividend = sgqlc.types.Field('CurrencyAmount', graphql_name='usdDividend')


class AccountStakingDividends(sgqlc.types.Type):
    __schema__ = graphql_schema
    dividends = sgqlc.types.Field(sgqlc.types.list_of(AccountStakingDividend), graphql_name='dividends')
    paid_days_this_month = sgqlc.types.Field(Int, graphql_name='paidDaysThisMonth')
    stakes_completed = sgqlc.types.Field(Int, graphql_name='stakesCompleted')
    stakes_in_progress = sgqlc.types.Field(Int, graphql_name='stakesInProgress')
    total_dividends = sgqlc.types.Field('CurrencyAmount', graphql_name='totalDividends')


class AccountStakingStatement(sgqlc.types.Type):
    __schema__ = graphql_schema
    active_stakes = sgqlc.types.Field(Int, graphql_name='activeStakes')
    id = sgqlc.types.Field(ID, graphql_name='id')
    month = sgqlc.types.Field(Int, graphql_name='month')
    total_dividends = sgqlc.types.Field('CurrencyAmount', graphql_name='totalDividends')
    year = sgqlc.types.Field(Int, graphql_name='year')


class AccountTransaction(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(String, graphql_name='address')
    block_datetime = sgqlc.types.Field(String, graphql_name='blockDatetime')
    block_index = sgqlc.types.Field(Int, graphql_name='blockIndex')
    blockchain = sgqlc.types.Field(String, graphql_name='blockchain')
    confirmations = sgqlc.types.Field('Confirmations', graphql_name='confirmations')
    fiat_value = sgqlc.types.Field(Float, graphql_name='fiatValue')
    status = sgqlc.types.Field(String, graphql_name='status')
    txid = sgqlc.types.Field(String, graphql_name='txid')
    type = sgqlc.types.Field(String, graphql_name='type')
    value = sgqlc.types.Field('CurrencyAmount', graphql_name='value')


class AccountVolume(sgqlc.types.Type):
    __schema__ = graphql_schema
    daily = sgqlc.types.Field('AccountVolumePeriod', graphql_name='daily')
    maker_fee_rate = sgqlc.types.Field(Float, graphql_name='makerFeeRate')
    monthly = sgqlc.types.Field('AccountVolumePeriod', graphql_name='monthly')
    taker_fee_rate = sgqlc.types.Field(Float, graphql_name='takerFeeRate')
    yearly = sgqlc.types.Field('AccountVolumePeriod', graphql_name='yearly')


class AccountVolumePeriod(sgqlc.types.Type):
    __schema__ = graphql_schema
    account_limit = sgqlc.types.Field('CurrencyAmount', graphql_name='accountLimit')
    account_spend = sgqlc.types.Field('CurrencyAmount', graphql_name='accountSpend')
    account_volume = sgqlc.types.Field('CurrencyAmount', graphql_name='accountVolume')
    exchange_volume = sgqlc.types.Field('CurrencyAmount', graphql_name='exchangeVolume')


class Activity(sgqlc.types.Type):
    __schema__ = graphql_schema
    browser = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='browser')
    city = sgqlc.types.Field(String, graphql_name='city')
    country_code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='countryCode')
    inserted_at = sgqlc.types.Field(sgqlc.types.non_null(DateTime), graphql_name='insertedAt')
    ip_address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='ipAddress')
    platform = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='platform')
    status = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='status')
    type = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='type')


class AddressResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    city = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='city')
    country = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='country')
    full_address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='fullAddress')
    road_address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='roadAddress')
    state = sgqlc.types.Field(String, graphql_name='state')
    zip_code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='zipCode')


class ApiLimit(sgqlc.types.Type):
    __schema__ = graphql_schema
    max_calls_per_time_period = sgqlc.types.Field(Int, graphql_name='maxCallsPerTimePeriod')
    request_name = sgqlc.types.Field(String, graphql_name='requestName')
    time_period = sgqlc.types.Field(String, graphql_name='timePeriod')


class Asset(sgqlc.types.Type):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(String, graphql_name='blockchain')
    blockchain_precision = sgqlc.types.Field(Int, graphql_name='blockchainPrecision')
    deposit_precision = sgqlc.types.Field(Int, graphql_name='depositPrecision')
    hash = sgqlc.types.Field(Base16, graphql_name='hash')
    name = sgqlc.types.Field(String, graphql_name='name')
    symbol = sgqlc.types.Field(CurrencySymbol, graphql_name='symbol')
    withdrawal_precision = sgqlc.types.Field(Int, graphql_name='withdrawalPrecision')


class AssetNonces(sgqlc.types.Type):
    __schema__ = graphql_schema
    asset = sgqlc.types.Field(sgqlc.types.non_null(CurrencySymbol), graphql_name='asset')
    nonces = sgqlc.types.Field(sgqlc.types.list_of(Int), graphql_name='nonces')


class CanceledOrder(sgqlc.types.Type):
    __schema__ = graphql_schema
    order_id = sgqlc.types.Field(ID, graphql_name='orderId')


class CanceledOrders(sgqlc.types.Type):
    __schema__ = graphql_schema
    accepted = sgqlc.types.Field(Boolean, graphql_name='accepted')


class Candle(sgqlc.types.Type):
    __schema__ = graphql_schema
    a_volume = sgqlc.types.Field('CurrencyAmount', graphql_name='aVolume')
    b_volume = sgqlc.types.Field('CurrencyAmount', graphql_name='bVolume')
    close_price = sgqlc.types.Field('CurrencyPrice', graphql_name='closePrice')
    high_price = sgqlc.types.Field('CurrencyPrice', graphql_name='highPrice')
    interval = sgqlc.types.Field(CandleInterval, graphql_name='interval')
    interval_starting_at = sgqlc.types.Field(DateTime, graphql_name='intervalStartingAt')
    low_price = sgqlc.types.Field('CurrencyPrice', graphql_name='lowPrice')
    open_price = sgqlc.types.Field('CurrencyPrice', graphql_name='openPrice')


class CandleRange(sgqlc.types.Type):
    __schema__ = graphql_schema
    candles = sgqlc.types.Field(sgqlc.types.list_of(Candle), graphql_name='candles')
    market = sgqlc.types.Field('Market', graphql_name='market')


class Confirmations(sgqlc.types.Type):
    __schema__ = graphql_schema
    denominator = sgqlc.types.Field(Int, graphql_name='denominator')
    numerator = sgqlc.types.Field(Int, graphql_name='numerator')


class CreateTwoFactorResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    two_factor_code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='twoFactorCode')
    uri_string = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='uriString')


class CurrencyAmount(sgqlc.types.Type):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(CurrencyNumber, graphql_name='amount')
    currency = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')


class CurrencyPrice(sgqlc.types.Type):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(CurrencyNumber, graphql_name='amount')
    currency_a = sgqlc.types.Field(CurrencySymbol, graphql_name='currencyA')
    currency_b = sgqlc.types.Field(CurrencySymbol, graphql_name='currencyB')


class ExchangeAssetMetric(sgqlc.types.Type):
    __schema__ = graphql_schema
    asset = sgqlc.types.Field(Asset, graphql_name='asset')
    asset_symbol = sgqlc.types.Field(CurrencySymbol, graphql_name='assetSymbol')
    circulating_supply = sgqlc.types.Field(CurrencyAmount, graphql_name='circulatingSupply')
    close_price_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='closePriceUsd')
    id = sgqlc.types.Field(ID, graphql_name='id')
    period = sgqlc.types.Field(MetricPeriod, graphql_name='period')
    price_change_percent = sgqlc.types.Field(Float, graphql_name='priceChangePercent')
    price_change_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='priceChangeUsd')
    price_history = sgqlc.types.Field(sgqlc.types.list_of('PricePoint'), graphql_name='priceHistory')
    volume = sgqlc.types.Field(CurrencyAmount, graphql_name='volume')
    volume_usd = sgqlc.types.Field(CurrencyAmount, graphql_name='volumeUsd')


class ExchangeMarketMetric(sgqlc.types.Type):
    __schema__ = graphql_schema
    close_price = sgqlc.types.Field(CurrencyPrice, graphql_name='closePrice')
    close_price_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='closePriceUsd')
    high_price = sgqlc.types.Field(CurrencyPrice, graphql_name='highPrice')
    high_price_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='highPriceUsd')
    id = sgqlc.types.Field(ID, graphql_name='id')
    low_price = sgqlc.types.Field(CurrencyPrice, graphql_name='lowPrice')
    low_price_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='lowPriceUsd')
    market = sgqlc.types.Field('Market', graphql_name='market')
    market_name = sgqlc.types.Field(MarketName, graphql_name='marketName')
    open_price = sgqlc.types.Field(CurrencyPrice, graphql_name='openPrice')
    open_price_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='openPriceUsd')
    period = sgqlc.types.Field(MetricPeriod, graphql_name='period')
    price_change_percent = sgqlc.types.Field(Float, graphql_name='priceChangePercent')
    price_change_usd = sgqlc.types.Field(CurrencyPrice, graphql_name='priceChangeUsd')
    price_history = sgqlc.types.Field(sgqlc.types.list_of('PricePoint'), graphql_name='priceHistory')
    volume = sgqlc.types.Field(CurrencyAmount, graphql_name='volume')
    volume_usd = sgqlc.types.Field(CurrencyAmount, graphql_name='volumeUsd')


class ExchangePublicKey(sgqlc.types.Type):
    __schema__ = graphql_schema
    public_key = sgqlc.types.Field(Base16, graphql_name='publicKey')
    timestamp = sgqlc.types.Field(Int, graphql_name='timestamp')


class ExchangeStatus(sgqlc.types.Type):
    __schema__ = graphql_schema
    api_limits = sgqlc.types.Field(sgqlc.types.list_of(ApiLimit), graphql_name='apiLimits')
    server_timestamp = sgqlc.types.Field(Int, graphql_name='serverTimestamp')
    status = sgqlc.types.Field(String, graphql_name='status')


class FiatGatewayCredential(sgqlc.types.Type):
    __schema__ = graphql_schema
    id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='id')
    partner_account_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerAccountId')
    partner_account_key = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerAccountKey')
    partner_id = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='partnerId')


class GetOrdersForMovementResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    asset_nonce = sgqlc.types.Field(Int, graphql_name='assetNonce')
    recycled_orders = sgqlc.types.Field(sgqlc.types.list_of('RecycledOrder'), graphql_name='recycledOrders')


class GetStatesResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    recycled_orders = sgqlc.types.Field(sgqlc.types.list_of('RecycledOrder'), graphql_name='recycledOrders')
    states = sgqlc.types.Field(sgqlc.types.list_of('State'), graphql_name='states')


class GraphPoint(sgqlc.types.Type):
    __schema__ = graphql_schema
    time = sgqlc.types.Field(DateTime, graphql_name='time')
    value = sgqlc.types.Field(Float, graphql_name='value')


class IdentityResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    account_id = sgqlc.types.Field(ID, graphql_name='accountId')
    country_code = sgqlc.types.Field(String, graphql_name='countryCode')
    country_match = sgqlc.types.Field(Boolean, graphql_name='countryMatch')
    country_status = sgqlc.types.Field(CountryStatusEnum, graphql_name='countryStatus')
    full_name = sgqlc.types.Field(String, graphql_name='fullName')
    id = sgqlc.types.Field(ID, graphql_name='id')
    state_code = sgqlc.types.Field(String, graphql_name='stateCode')
    verification_level = sgqlc.types.Field(VerificationLevelEnum, graphql_name='verificationLevel')


class KeysResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    encrypted_secret_key = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKey')
    encrypted_secret_key_nonce = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyNonce')
    encrypted_secret_key_tag = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyTag')
    signature_public_key = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='signaturePublicKey')


class KeysWalletsResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    encrypted_secret_key = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKey')
    encrypted_secret_key_nonce = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyNonce')
    encrypted_secret_key_tag = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyTag')
    signature_public_key = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='signaturePublicKey')
    wallets = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of('Wallet')), graphql_name='wallets')


class KycCreateTokenResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    token = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='token')


class KycGetRedirectUrlResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    redirect_url = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='redirectUrl')


class KycIdentityResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='address')
    birthdate = sgqlc.types.Field(sgqlc.types.non_null(Date), graphql_name='birthdate')
    country_code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='countryCode')
    full_name = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='fullName')


class KycStatusResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    details = sgqlc.types.Field(KycErrorDetailsEnum, graphql_name='details')
    status = sgqlc.types.Field(sgqlc.types.non_null(KycStatusEnum), graphql_name='status')


class KycSubmitScanReferenceResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    scan_reference = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='scanReference')


class ListAccountTransactionsResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    next_cursor = sgqlc.types.Field(PaginationCursor, graphql_name='nextCursor')
    transactions = sgqlc.types.Field(sgqlc.types.list_of(AccountTransaction), graphql_name='transactions')


class LocationResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    country_code = sgqlc.types.Field(String, graphql_name='countryCode')
    region_code = sgqlc.types.Field(String, graphql_name='regionCode')


class Market(sgqlc.types.Type):
    __schema__ = graphql_schema
    a_asset = sgqlc.types.Field(Asset, graphql_name='aAsset')
    a_unit = sgqlc.types.Field(CurrencySymbol, graphql_name='aUnit')
    a_unit_precision = sgqlc.types.Field(Int, graphql_name='aUnitPrecision')
    b_asset = sgqlc.types.Field(Asset, graphql_name='bAsset')
    b_unit = sgqlc.types.Field(CurrencySymbol, graphql_name='bUnit')
    b_unit_precision = sgqlc.types.Field(Int, graphql_name='bUnitPrecision')
    id = sgqlc.types.Field(ID, graphql_name='id')
    min_tick_size = sgqlc.types.Field(String, graphql_name='minTickSize')
    min_trade_increment = sgqlc.types.Field(String, graphql_name='minTradeIncrement')
    min_trade_increment_b = sgqlc.types.Field(String, graphql_name='minTradeIncrementB')
    min_trade_size = sgqlc.types.Field(String, graphql_name='minTradeSize')
    min_trade_size_b = sgqlc.types.Field(String, graphql_name='minTradeSizeB')
    name = sgqlc.types.Field(MarketName, graphql_name='name')
    price_granularity = sgqlc.types.Field(Int, graphql_name='priceGranularity')
    primary = sgqlc.types.Field(Boolean, graphql_name='primary')
    status = sgqlc.types.Field(MarketStatus, graphql_name='status')


class Movement(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(String, graphql_name='address')
    blockchain = sgqlc.types.Field(Blockchain, graphql_name='blockchain')
    confirmations = sgqlc.types.Field(Int, graphql_name='confirmations')
    currency = sgqlc.types.Field(CurrencySymbol, graphql_name='currency')
    fee = sgqlc.types.Field(String, graphql_name='fee')
    id = sgqlc.types.Field(String, graphql_name='id')
    nonce = sgqlc.types.Field(Int, graphql_name='nonce')
    public_key = sgqlc.types.Field(Base16, graphql_name='publicKey')
    quantity = sgqlc.types.Field(CurrencyAmount, graphql_name='quantity')
    received_at = sgqlc.types.Field(DateTime, graphql_name='receivedAt')
    signature = sgqlc.types.Field(Base16, graphql_name='signature')
    status = sgqlc.types.Field(MovementStatus, graphql_name='status')
    target_address = sgqlc.types.Field(String, graphql_name='targetAddress')
    transaction_hash = sgqlc.types.Field(Base16, graphql_name='transactionHash')
    transaction_payload = sgqlc.types.Field(Base16, graphql_name='transactionPayload')
    type = sgqlc.types.Field(MovementType, graphql_name='type')


class Options(sgqlc.types.Type):
    __schema__ = graphql_schema
    web_kyc_enabled = sgqlc.types.Field(Boolean, graphql_name='webKycEnabled')


class Order(sgqlc.types.Type):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(CurrencyAmount, graphql_name='amount')
    amount_executed = sgqlc.types.Field(CurrencyAmount, graphql_name='amountExecuted')
    amount_remaining = sgqlc.types.Field(CurrencyAmount, graphql_name='amountRemaining')
    buy_or_sell = sgqlc.types.Field(OrderBuyOrSell, graphql_name='buyOrSell')
    cancel_at = sgqlc.types.Field(DateTime, graphql_name='cancelAt')
    cancellation_policy = sgqlc.types.Field(OrderCancellationPolicy, graphql_name='cancellationPolicy')
    cancellation_reason = sgqlc.types.Field(OrderCancellationReason, graphql_name='cancellationReason')
    id = sgqlc.types.Field(ID, graphql_name='id')
    limit_price = sgqlc.types.Field(CurrencyPrice, graphql_name='limitPrice')
    market = sgqlc.types.Field(Market, graphql_name='market')
    placed_at = sgqlc.types.Field(DateTime, graphql_name='placedAt')
    status = sgqlc.types.Field(OrderStatus, graphql_name='status')
    stop_price = sgqlc.types.Field(CurrencyPrice, graphql_name='stopPrice')
    trades = sgqlc.types.Field(sgqlc.types.list_of('Trade'), graphql_name='trades')
    type = sgqlc.types.Field(OrderType, graphql_name='type')


class OrderBook(sgqlc.types.Type):
    __schema__ = graphql_schema
    asks = sgqlc.types.Field(sgqlc.types.list_of('OrderBookRecord'), graphql_name='asks')
    bids = sgqlc.types.Field(sgqlc.types.list_of('OrderBookRecord'), graphql_name='bids')
    market = sgqlc.types.Field(Market, graphql_name='market')
    update_id = sgqlc.types.Field(Int, graphql_name='updateId')


class OrderBookRecord(sgqlc.types.Type):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(CurrencyAmount, graphql_name='amount')
    price = sgqlc.types.Field(CurrencyPrice, graphql_name='price')


class OrderHistory(sgqlc.types.Type):
    __schema__ = graphql_schema
    next = sgqlc.types.Field(PaginationCursor, graphql_name='next')
    orders = sgqlc.types.Field(sgqlc.types.list_of(Order), graphql_name='orders')


class OrderPlaced(sgqlc.types.Type):
    __schema__ = graphql_schema
    id = sgqlc.types.Field(ID, graphql_name='id')
    status = sgqlc.types.Field(OrderStatus, graphql_name='status')


class PaginatedActivity(sgqlc.types.Type):
    __schema__ = graphql_schema
    entries = sgqlc.types.Field(sgqlc.types.list_of(Activity), graphql_name='entries')
    metadata = sgqlc.types.Field(sgqlc.types.non_null('PaginationMetadata'), graphql_name='metadata')


class PaginationMetadata(sgqlc.types.Type):
    __schema__ = graphql_schema
    after = sgqlc.types.Field(String, graphql_name='after')
    before = sgqlc.types.Field(String, graphql_name='before')
    limit = sgqlc.types.Field(Int, graphql_name='limit')
    total_count = sgqlc.types.Field(Int, graphql_name='totalCount')


class PricePoint(sgqlc.types.Type):
    __schema__ = graphql_schema
    at = sgqlc.types.Field(DateTime, graphql_name='at')
    price = sgqlc.types.Field(CurrencyPrice, graphql_name='price')


class RecycledOrder(sgqlc.types.Type):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(Blockchain, graphql_name='blockchain')
    message = sgqlc.types.Field(Base16, graphql_name='message')


class ReferralCode(sgqlc.types.Type):
    __schema__ = graphql_schema
    code = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='code')


class ReferralRecipientsTicket(sgqlc.types.Type):
    __schema__ = graphql_schema
    activated = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='activated')
    full_name = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='fullName')
    pending = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='pending')


class RestrictedJurisdictionResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    banned = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(String)), graphql_name='banned')
    restricted = sgqlc.types.Field(sgqlc.types.non_null(sgqlc.types.list_of(String)), graphql_name='restricted')


class RootMutationType(sgqlc.types.Type):
    __schema__ = graphql_schema
    lost_password = sgqlc.types.Field('SuccessMessage', graphql_name='lostPassword', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='email', default=None)),
    ))
    )
    update_account_settings = sgqlc.types.Field(AccountResponse, graphql_name='updateAccountSettings', args=sgqlc.types.ArgDict((
        ('settings', sgqlc.types.Arg(sgqlc.types.non_null(Json), graphql_name='settings', default=None)),
    ))
    )
    add_movement = sgqlc.types.Field(Movement, graphql_name='addMovement', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(AddMovementParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    sign_out = sgqlc.types.Field(Boolean, graphql_name='signOut')
    update_password = sgqlc.types.Field(KeysResponse, graphql_name='updatePassword', args=sgqlc.types.ArgDict((
        ('encrypted_secret_key', sgqlc.types.Arg(String, graphql_name='encryptedSecretKey', default=None)),
        ('encrypted_secret_key_nonce', sgqlc.types.Arg(String, graphql_name='encryptedSecretKeyNonce', default=None)),
        ('encrypted_secret_key_tag', sgqlc.types.Arg(String, graphql_name='encryptedSecretKeyTag', default=None)),
        ('new_password', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='newPassword', default=None)),
        ('old_password', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='oldPassword', default=None)),
        ('two_fa', sgqlc.types.Arg(String, graphql_name='twoFa', default=None)),
    ))
    )
    kyc_get_redirect_url = sgqlc.types.Field(KycGetRedirectUrlResponse, graphql_name='kycGetRedirectUrl')
    add_keys_with_wallets = sgqlc.types.Field(KeysWalletsResponse, graphql_name='addKeysWithWallets', args=sgqlc.types.ArgDict((
        ('encrypted_secret_key', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='encryptedSecretKey', default=None)),
        ('encrypted_secret_key_nonce', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyNonce', default=None)),
        ('encrypted_secret_key_tag', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='encryptedSecretKeyTag', default=None)),
        ('signature_public_key', sgqlc.types.Arg(sgqlc.types.non_null(Base16), graphql_name='signaturePublicKey', default=None)),
        ('wallets', sgqlc.types.Arg(sgqlc.types.list_of(WalletInput), graphql_name='wallets', default=None)),
    ))
    )
    update_movement = sgqlc.types.Field(Movement, graphql_name='updateMovement', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(UpdateMovementParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    update_identity_add_country_and_state = sgqlc.types.Field(IdentityResponse, graphql_name='updateIdentityAddCountryAndState', args=sgqlc.types.ArgDict((
        ('country_code', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='countryCode', default=None)),
        ('state_code', sgqlc.types.Arg(String, graphql_name='stateCode', default=None)),
    ))
    )
    kyc_create_token = sgqlc.types.Field(KycCreateTokenResponse, graphql_name='kycCreateToken')
    update_email = sgqlc.types.Field('SuccessMessage', graphql_name='updateEmail', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='email', default=None)),
        ('two_fa', sgqlc.types.Arg(String, graphql_name='twoFa', default=None)),
    ))
    )
    get_states = sgqlc.types.Field(GetStatesResponse, graphql_name='getStates', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetStatesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    sign_states = sgqlc.types.Field('SignStatesResponse', graphql_name='signStates', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(SignStatesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    place_stop_limit_order = sgqlc.types.Field(OrderPlaced, graphql_name='placeStopLimitOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(PlaceStopLimitOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    kyc_update_identity = sgqlc.types.Field(KycIdentityResponse, graphql_name='kycUpdateIdentity', args=sgqlc.types.ArgDict((
        ('address', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='address', default=None)),
        ('birthdate', sgqlc.types.Arg(sgqlc.types.non_null(Date), graphql_name='birthdate', default=None)),
        ('full_name', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='fullName', default=None)),
    ))
    )
    resend_verification_email = sgqlc.types.Field('SuccessMessage', graphql_name='resendVerificationEmail', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='email', default=None)),
    ))
    )
    create_two_factor = sgqlc.types.Field(CreateTwoFactorResponse, graphql_name='createTwoFactor')
    cancel_order = sgqlc.types.Field(CanceledOrder, graphql_name='cancelOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(CancelOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    place_market_order = sgqlc.types.Field(OrderPlaced, graphql_name='placeMarketOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(PlaceMarketOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    verification_skip = sgqlc.types.Field(String, graphql_name='verificationSkip', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(String, graphql_name='email', default=None)),
    ))
    )
    complete_account = sgqlc.types.Field(AccountKeysAndWalletsResponse, graphql_name='completeAccount')
    cancel_all_orders = sgqlc.types.Field(CanceledOrders, graphql_name='cancelAllOrders', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(CancelAllOrdersParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    two_factor_login = sgqlc.types.Field('TwoFactorLoginResponse', graphql_name='twoFactorLogin', args=sgqlc.types.ArgDict((
        ('two_fa', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='twoFa', default=None)),
    ))
    )
    add_fiat_gateway_credentials = sgqlc.types.Field(FiatGatewayCredential, graphql_name='addFiatGatewayCredentials', args=sgqlc.types.ArgDict((
        ('credentials', sgqlc.types.Arg(sgqlc.types.non_null(FiatGatewayCredentialInput), graphql_name='credentials', default=None)),
    ))
    )
    sign_in = sgqlc.types.Field('SignInResponse', graphql_name='signIn', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='email', default=None)),
        ('password', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='password', default=None)),
    ))
    )
    enable_two_factor = sgqlc.types.Field(AccountResponse, graphql_name='enableTwoFactor', args=sgqlc.types.ArgDict((
        ('two_fa', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='twoFa', default=None)),
    ))
    )
    refresh_token = sgqlc.types.Field('SuccessMessage', graphql_name='refreshToken')
    sync_states = sgqlc.types.Field('SyncStatesResponse', graphql_name='syncStates', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(SyncStatesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    place_stop_market_order = sgqlc.types.Field(OrderPlaced, graphql_name='placeStopMarketOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(PlaceStopMarketOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    kyc_submit_scan_reference = sgqlc.types.Field(KycSubmitScanReferenceResponse, graphql_name='kycSubmitScanReference', args=sgqlc.types.ArgDict((
        ('scan_reference', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='scanReference', default=None)),
    ))
    )
    place_limit_order = sgqlc.types.Field(OrderPlaced, graphql_name='placeLimitOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(PlaceLimitOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    verify_hash = sgqlc.types.Field('SuccessMessage', graphql_name='verifyHash', args=sgqlc.types.ArgDict((
        ('email', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='email', default=None)),
        ('options', sgqlc.types.Arg(VerificationOptions, graphql_name='options', default=None)),
        ('verification_hash', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='verificationHash', default=None)),
    ))
    )
    disable_two_factor = sgqlc.types.Field(AccountResponse, graphql_name='disableTwoFactor', args=sgqlc.types.ArgDict((
        ('two_fa', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='twoFa', default=None)),
    ))
    )
    sign_up = sgqlc.types.Field('SuccessMessage', graphql_name='signUp', args=sgqlc.types.ArgDict((
        ('account', sgqlc.types.Arg(SignUpInput, graphql_name='account', default=None)),
        ('recaptcha', sgqlc.types.Arg(String, graphql_name='recaptcha', default=None)),
        ('referral_code', sgqlc.types.Arg(String, graphql_name='referralCode', default=None)),
    ))
    )


class RootQueryType(sgqlc.types.Type):
    __schema__ = graphql_schema
    list_trades = sgqlc.types.Field('TradeHistory', graphql_name='listTrades', args=sgqlc.types.ArgDict((
        ('before', sgqlc.types.Arg(PaginationCursor, graphql_name='before', default=None)),
        ('limit', sgqlc.types.Arg(Int, graphql_name='limit', default=50)),
        ('market_name', sgqlc.types.Arg(MarketName, graphql_name='marketName', default=None)),
    ))
    )
    get_assets_nonces = sgqlc.types.Field(sgqlc.types.list_of(AssetNonces), graphql_name='getAssetsNonces', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetAssetsNoncesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_orders_for_movement = sgqlc.types.Field(GetOrdersForMovementResponse, graphql_name='getOrdersForMovement', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetOrdersForMovementParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    list_tickers = sgqlc.types.Field(sgqlc.types.list_of('Ticker'), graphql_name='listTickers')
    get_account_portfolio = sgqlc.types.Field(AccountPortfolio, graphql_name='getAccountPortfolio', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetAccountPortfolioParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_referral_recipients_tickets = sgqlc.types.Field(sgqlc.types.list_of(ReferralRecipientsTicket), graphql_name='getReferralRecipientsTickets')
    location = sgqlc.types.Field(LocationResponse, graphql_name='location')
    get_account_order = sgqlc.types.Field(Order, graphql_name='getAccountOrder', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetAccountOrderParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_tickets_count = sgqlc.types.Field('TicketsCount', graphql_name='getTicketsCount')
    list_assets = sgqlc.types.Field(sgqlc.types.list_of(Asset), graphql_name='listAssets')
    get_deposit_address = sgqlc.types.Field(AccountDepositAddress, graphql_name='getDepositAddress', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetDepositAddressParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_movement = sgqlc.types.Field(Movement, graphql_name='getMovement', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetMovementParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    list_activity = sgqlc.types.Field(PaginatedActivity, graphql_name='listActivity', args=sgqlc.types.ArgDict((
        ('pagination_options', sgqlc.types.Arg(PaginationOptions, graphql_name='paginationOptions', default=None)),
    ))
    )
    list_exchange_asset_metrics = sgqlc.types.Field(sgqlc.types.list_of(ExchangeAssetMetric), graphql_name='listExchangeAssetMetrics', args=sgqlc.types.ArgDict((
        ('period', sgqlc.types.Arg(sgqlc.types.non_null(MetricPeriod), graphql_name='period', default=None)),
    ))
    )
    get_identity = sgqlc.types.Field(IdentityResponse, graphql_name='getIdentity')
    get_server_encryption_key = sgqlc.types.Field(String, graphql_name='getServerEncryptionKey', args=sgqlc.types.ArgDict((
        ('password', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='password', default=None)),
        ('two_fa', sgqlc.types.Arg(String, graphql_name='twoFa', default=None)),
    ))
    )
    get_exchange_status = sgqlc.types.Field(ExchangeStatus, graphql_name='getExchangeStatus')
    list_account_stakes = sgqlc.types.Field(AccountStakes, graphql_name='listAccountStakes', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountStakesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_account_volumes = sgqlc.types.Field(AccountVolume, graphql_name='getAccountVolumes', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetAccountVolumesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    list_markets = sgqlc.types.Field(sgqlc.types.list_of(Market), graphql_name='listMarkets')
    list_candles = sgqlc.types.Field(CandleRange, graphql_name='listCandles', args=sgqlc.types.ArgDict((
        ('before', sgqlc.types.Arg(DateTime, graphql_name='before', default=None)),
        ('interval', sgqlc.types.Arg(CandleInterval, graphql_name='interval', default='ONE_MINUTE')),
        ('limit', sgqlc.types.Arg(Int, graphql_name='limit', default=25)),
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    valid_referral_code = sgqlc.types.Field(Boolean, graphql_name='validReferralCode', args=sgqlc.types.ArgDict((
        ('referral_code', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='referralCode', default=None)),
    ))
    )
    get_market = sgqlc.types.Field(Market, graphql_name='getMarket', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    list_account_staking_statements = sgqlc.types.Field(sgqlc.types.list_of(AccountStakingStatement), graphql_name='listAccountStakingStatements', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountStakingStatementsParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    list_account_balances = sgqlc.types.Field(sgqlc.types.list_of(AccountBalance), graphql_name='listAccountBalances', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountBalancesParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    validate_address = sgqlc.types.Field('ValidateAddressResponse', graphql_name='validateAddress', args=sgqlc.types.ArgDict((
        ('free_form_address', sgqlc.types.Arg(sgqlc.types.non_null(String), graphql_name='freeFormAddress', default=None)),
    ))
    )
    get_ticker = sgqlc.types.Field('Ticker', graphql_name='getTicker', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    get_account_keys_and_wallets = sgqlc.types.Field(AccountKeysAndWalletsResponse, graphql_name='getAccountKeysAndWallets')
    list_exchange_market_metrics = sgqlc.types.Field(sgqlc.types.list_of(ExchangeMarketMetric), graphql_name='listExchangeMarketMetrics', args=sgqlc.types.ArgDict((
        ('period', sgqlc.types.Arg(sgqlc.types.non_null(MetricPeriod), graphql_name='period', default=None)),
    ))
    )
    get_order_book = sgqlc.types.Field(OrderBook, graphql_name='getOrderBook', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    trusted_ip = sgqlc.types.Field(Boolean, graphql_name='trustedIp')
    list_movements = sgqlc.types.Field(sgqlc.types.list_of(Movement), graphql_name='listMovements', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListMovementsParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_fiat_gateway_credentials = sgqlc.types.Field(sgqlc.types.list_of(FiatGatewayCredential), graphql_name='getFiatGatewayCredentials')
    list_account_transactions = sgqlc.types.Field(ListAccountTransactionsResponse, graphql_name='listAccountTransactions', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountTransactionsParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    get_account = sgqlc.types.Field(AccountResponse, graphql_name='getAccount')
    kyc_status = sgqlc.types.Field(KycStatusResponse, graphql_name='kycStatus')
    get_account_balance = sgqlc.types.Field(AccountBalance, graphql_name='getAccountBalance', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(GetAccountBalanceParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    list_account_orders = sgqlc.types.Field(OrderHistory, graphql_name='listAccountOrders', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountOrdersParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )
    location_restrictions = sgqlc.types.Field(sgqlc.types.list_of(sgqlc.types.list_of(RestrictionEnum)), graphql_name='locationRestrictions', args=sgqlc.types.ArgDict((
        ('country_code', sgqlc.types.Arg(String, graphql_name='countryCode', default=None)),
        ('state_code', sgqlc.types.Arg(String, graphql_name='stateCode', default=None)),
    ))
    )
    get_exchange_public_key = sgqlc.types.Field(ExchangePublicKey, graphql_name='getExchangePublicKey')
    restricted_jurisdiction = sgqlc.types.Field(RestrictedJurisdictionResponse, graphql_name='restrictedJurisdiction')
    get_referral_codes = sgqlc.types.Field(sgqlc.types.list_of(ReferralCode), graphql_name='getReferralCodes')
    list_orders = sgqlc.types.Field(OrderHistory, graphql_name='listOrders', args=sgqlc.types.ArgDict((
        ('before', sgqlc.types.Arg(PaginationCursor, graphql_name='before', default=None)),
        ('buy_or_sell', sgqlc.types.Arg(OrderBuyOrSell, graphql_name='buyOrSell', default=None)),
        ('limit', sgqlc.types.Arg(Int, graphql_name='limit', default=50)),
        ('market_name', sgqlc.types.Arg(MarketName, graphql_name='marketName', default=None)),
        ('range_start', sgqlc.types.Arg(DateTime, graphql_name='rangeStart', default=None)),
        ('range_stop', sgqlc.types.Arg(DateTime, graphql_name='rangeStop', default=None)),
        ('status', sgqlc.types.Arg(sgqlc.types.list_of(OrderStatus), graphql_name='status', default=None)),
        ('type', sgqlc.types.Arg(sgqlc.types.list_of(OrderType), graphql_name='type', default=None)),
    ))
    )
    list_account_staking_dividends = sgqlc.types.Field(AccountStakingDividends, graphql_name='listAccountStakingDividends', args=sgqlc.types.ArgDict((
        ('payload', sgqlc.types.Arg(sgqlc.types.non_null(ListAccountStakingDividendsParams), graphql_name='payload', default=None)),
        ('signature', sgqlc.types.Arg(sgqlc.types.non_null(Signature), graphql_name='signature', default=None)),
    ))
    )


class RootSubscriptionType(sgqlc.types.Type):
    __schema__ = graphql_schema
    kyc_status_update = sgqlc.types.Field(KycStatusResponse, graphql_name='kycStatusUpdate')
    new_trades = sgqlc.types.Field(sgqlc.types.list_of('Trade'), graphql_name='newTrades', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    updated_candles = sgqlc.types.Field(sgqlc.types.list_of(Candle), graphql_name='updatedCandles', args=sgqlc.types.ArgDict((
        ('interval', sgqlc.types.Arg(sgqlc.types.non_null(CandleInterval), graphql_name='interval', default=None)),
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    updated_exchange_asset_metrics = sgqlc.types.Field(sgqlc.types.list_of(ExchangeAssetMetric), graphql_name='updatedExchangeAssetMetrics', args=sgqlc.types.ArgDict((
        ('period', sgqlc.types.Arg(sgqlc.types.non_null(MetricPeriod), graphql_name='period', default=None)),
    ))
    )
    updated_exchange_market_metrics = sgqlc.types.Field(sgqlc.types.list_of(ExchangeMarketMetric), graphql_name='updatedExchangeMarketMetrics', args=sgqlc.types.ArgDict((
        ('period', sgqlc.types.Arg(sgqlc.types.non_null(MetricPeriod), graphql_name='period', default=None)),
    ))
    )
    updated_order_book = sgqlc.types.Field(OrderBook, graphql_name='updatedOrderBook', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    updated_ticker = sgqlc.types.Field('Ticker', graphql_name='updatedTicker', args=sgqlc.types.ArgDict((
        ('market_name', sgqlc.types.Arg(sgqlc.types.non_null(MarketName), graphql_name='marketName', default=None)),
    ))
    )
    updated_tickers = sgqlc.types.Field(sgqlc.types.list_of('Ticker'), graphql_name='updatedTickers')


class ServerSignedState(sgqlc.types.Type):
    __schema__ = graphql_schema
    blockchain = sgqlc.types.Field(Blockchain, graphql_name='blockchain')
    message = sgqlc.types.Field(Base16, graphql_name='message')


class SignInResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    account = sgqlc.types.Field(AccountKeysAndWalletsResponse, graphql_name='account')
    server_encryption_key = sgqlc.types.Field(String, graphql_name='serverEncryptionKey')
    two_fa_required = sgqlc.types.Field(sgqlc.types.non_null(Boolean), graphql_name='twoFaRequired')


class SignStatesResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    server_signed_states = sgqlc.types.Field(sgqlc.types.list_of(ServerSignedState), graphql_name='serverSignedStates')


class State(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(String, graphql_name='address')
    balance = sgqlc.types.Field(CurrencyAmount, graphql_name='balance')
    blockchain = sgqlc.types.Field(Blockchain, graphql_name='blockchain')
    message = sgqlc.types.Field(Base16, graphql_name='message')
    nonce = sgqlc.types.Field(Int, graphql_name='nonce')


class SuccessMessage(sgqlc.types.Type):
    __schema__ = graphql_schema
    message = sgqlc.types.Field(String, graphql_name='message')
    slug = sgqlc.types.Field(String, graphql_name='slug')


class SyncStatesResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    result = sgqlc.types.Field(String, graphql_name='result')


class Ticker(sgqlc.types.Type):
    __schema__ = graphql_schema
    best_ask_price = sgqlc.types.Field(CurrencyPrice, graphql_name='bestAskPrice')
    best_ask_size = sgqlc.types.Field(CurrencyAmount, graphql_name='bestAskSize')
    best_bid_price = sgqlc.types.Field(CurrencyPrice, graphql_name='bestBidPrice')
    best_bid_size = sgqlc.types.Field(CurrencyAmount, graphql_name='bestBidSize')
    high_price24h = sgqlc.types.Field(CurrencyPrice, graphql_name='highPrice24h')
    id = sgqlc.types.Field(ID, graphql_name='id')
    last_price = sgqlc.types.Field(CurrencyPrice, graphql_name='lastPrice')
    low_price24h = sgqlc.types.Field(CurrencyPrice, graphql_name='lowPrice24h')
    market = sgqlc.types.Field(Market, graphql_name='market')
    market_name = sgqlc.types.Field(MarketName, graphql_name='marketName')
    price_change24h = sgqlc.types.Field(CurrencyPrice, graphql_name='priceChange24h')
    price_change24h_pct = sgqlc.types.Field(Float, graphql_name='priceChange24hPct')
    usd_last_price = sgqlc.types.Field(CurrencyPrice, graphql_name='usdLastPrice')
    usd_last_price_b = sgqlc.types.Field(CurrencyPrice, graphql_name='usdLastPriceB')
    volume24h = sgqlc.types.Field(CurrencyAmount, graphql_name='volume24h')


class TicketsCount(sgqlc.types.Type):
    __schema__ = graphql_schema
    activated = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='activated')
    pending = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='pending')


class Trade(sgqlc.types.Type):
    __schema__ = graphql_schema
    amount = sgqlc.types.Field(CurrencyAmount, graphql_name='amount')
    cursor = sgqlc.types.Field(String, graphql_name='cursor')
    direction = sgqlc.types.Field(Direction, graphql_name='direction')
    executed_at = sgqlc.types.Field(DateTime, graphql_name='executedAt')
    id = sgqlc.types.Field(ID, graphql_name='id')
    limit_price = sgqlc.types.Field(CurrencyPrice, graphql_name='limitPrice')
    maker_fee = sgqlc.types.Field(CurrencyAmount, graphql_name='makerFee')
    maker_gave = sgqlc.types.Field(CurrencyAmount, graphql_name='makerGave')
    maker_order_id = sgqlc.types.Field(ID, graphql_name='makerOrderId')
    maker_received = sgqlc.types.Field(CurrencyAmount, graphql_name='makerReceived')
    market = sgqlc.types.Field(Market, graphql_name='market')
    taker_fee = sgqlc.types.Field(CurrencyAmount, graphql_name='takerFee')
    taker_gave = sgqlc.types.Field(CurrencyAmount, graphql_name='takerGave')
    taker_order_id = sgqlc.types.Field(ID, graphql_name='takerOrderId')
    taker_received = sgqlc.types.Field(CurrencyAmount, graphql_name='takerReceived')
    usd_arate = sgqlc.types.Field(CurrencyPrice, graphql_name='usdARate')
    usd_brate = sgqlc.types.Field(CurrencyPrice, graphql_name='usdBRate')


class TradeHistory(sgqlc.types.Type):
    __schema__ = graphql_schema
    next = sgqlc.types.Field(PaginationCursor, graphql_name='next')
    trades = sgqlc.types.Field(sgqlc.types.list_of(Trade), graphql_name='trades')


class TwoFactorLoginResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    account = sgqlc.types.Field(sgqlc.types.non_null(AccountKeysAndWalletsResponse), graphql_name='account')
    server_encryption_key = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='serverEncryptionKey')


class ValidateAddressResponse(sgqlc.types.Type):
    __schema__ = graphql_schema
    results = sgqlc.types.Field(sgqlc.types.list_of(AddressResponse), graphql_name='results')


class Wallet(sgqlc.types.Type):
    __schema__ = graphql_schema
    address = sgqlc.types.Field(sgqlc.types.non_null(String), graphql_name='address')
    blockchain = sgqlc.types.Field(sgqlc.types.non_null(BlockchainName), graphql_name='blockchain')
    chain_index = sgqlc.types.Field(sgqlc.types.non_null(Int), graphql_name='chainIndex')
    public_key = sgqlc.types.Field(sgqlc.types.non_null(Base16), graphql_name='publicKey')


########################################################################
# Unions
########################################################################

from functools import wraps


def load_markets(func):
    """
    @load_markets decorator for API methods
    """
    @wraps(func)
    def func_wrapper(self, *args, **kwargs):
        self._load_markets()
        return func(self, *args, **kwargs)
    return func_wrapper
